# ResultsAnalysis # 

Module providing core functionality for data processing and plotting, based on pandas and matplotlib.

This group of python scripts was originally designed to extract, analyse and plot the results of
solubility/partition coefficient calculations, with a simple CLI designed for analysis.

## How do I get set up? ##

To build and install this module from source please follow the below instructions.

### Clone the repository ###

*From bitbucket:*

    git clone https://bitbucket.org/mdd31/resultsanalysis.git
    

*From University of Cambridge gitlab:*

    git clone git@gitlab.developers.cam.ac.uk:ch/hunter/ssiptools/utils/resultsanalysis.git    

Change to the repository: 

    cd resultsanalysis

### Setting up the python environment ###

Details of an anaconda environment is provided in the repository with the required dependencies for this package.

	conda env create -f environment.yml

This creates an environment called 'resultsanalysis' which can be loaded using:

	conda activate resultsanalysis


### Using pip to install module ###

To install in your environment using pip run:

	pip install .

This installs it in your current python environment.
Test that it has been installed:

    ~$ ipython
    Python 3.7.5 (default, Oct 25 2019, 15:51:11) 
    Type 'copyright', 'credits' or 'license' for more information
    IPython 7.10.2 -- An enhanced Interactive Python. Type '?' for help.

    In [1]: import resultsanalysis.dataclasses.molecule

### Expected usage ###

This module contains classes that act as wrappers for pandas dataframes, for the storage and manipulation of information.
Methods for outputting this information to file or the production of plots with matplotlib are also provided.

### Documentation ###

Documentation can be rendered using sphinx.

	cd docs
	make html

The rendered HTML can then be found in the build sub folder.

In progress: display of this documentation in a project wiki.

### Contribution guidelines ###

This code has been released under the AGPLv3 license.
If you find any bugs please file an issue ticket.
Submission of pull requests for open issues or improvements are welcomed.

### Who do I talk to? ###

Any queries please contact Mark Driver.

