#    resultsanalysis provides generic tools and classes for analysis of calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    resultsanalysis is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""Module to contain data for initialising plots
"""

import logging
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import scale as mscale
from matplotlib import cm
from matplotlib.lines import Line2D
from matplotlib import rc
import numpy as np
import numpy.polynomial.polynomial as poly
import resultsanalysis.resultsoutput.fgipscale as fgipscale


logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)


rc("font", **{"family": "sans-serif", "sans-serif": ["Helvetica"]})
# for Palatino and other serif fonts use:
# rc('font',**{'family':'serif','serif':['Palatino']})
rc("text", usetex=True)

# BUG: this example fails with any other setting of axisbelow
matplotlib.rcParams["axes.axisbelow"] = False
matplotlib.rcParams["text.usetex"] = True
matplotlib.rcParams["contour.negative_linestyle"] = "solid"
matplotlib.rcParams["font.family"] = "serif"
matplotlib.rcParams["font.serif"] = "cm"

FIGURE_SIZES = {
    "scatter plots": (7, 6),
    "horizontal bar chart": (8, 15),
    "vertical bar chart": (7, 6),
    "contour plot": (7, 7),
    "similarity matrix": (30, 30),
}

PLOT_SHAPES_FOR_RING_SIZE = {
    0: ["o", "b", "none"],
    1: ["o", "r", "none"],
    2: ["o", "g", "none"],
    3: ["o", "c", "none"],
    4: ["o", "m", "none"],
    5: ["v", "b", "none"],
    6: ["v", "r", "none"],
    7: ["v", "g", "none"],
    8: ["v", "c", "none"],
    9: ["v", "m", "none"],
}
PLOT_SHAPES_FOR_FUNC_GROUP = {
    "Primary_alcohol": ["o", "b", "none"],
    "Secondary_alcohol": ["o", "r", "none"],
    "Tertiary_alcohol": ["o", "g", "none"],
    "Phenol": ["o", "c", "none"],
    "Enol": ["o", "m", "none"],
    "Primary_aliph_amine": ["v", "b", "none"],
    "Secondary_aliph_amine": ["v", "r", "none"],
    "Tertiary_aliph_amine": ["v", "g", "none"],
    "Primary_Aniline": ["v", "c", "none"],
    "Secondary_Aniline": ["v", "m", "none"],
    "Aldehyde": ["^", "b", "none"],
    "Ketone": ["^", "r", "none"],
    "Ester": ["^", "g", "none"],
    "Carbonate": ["^", "c", "none"],
    "Tertiary_Aniline": ["^", "m", "none"],
    "Hemiacetal": [">", "b", "none"],
    "Hemiketal": [">", "r", "none"],
    "Acetal": [">", "g", "none"],
    "Ketal": [">", "c", "none"],
    "Orthoester": [">", "m", "none"],
}

mscale.register_scale(fgipscale.FGIPScale)
# LOGGER.debug(mscale.get_scale_names())
# LOGGER.debug(mscale.get_scale_docs())


def create_and_write_contour_plot(input_data, fileformat="svg", **kwargs):
    """This creates a contour plot and outputs it to file.
    """
    contour_plot, contour_plot_axis, contour_set = create_contour_plot(input_data)
    LOGGER.debug("y scale: %s", contour_plot_axis.get_yscale())
    LOGGER.debug("x scale: %s", contour_plot_axis.get_xscale())
    LOGGER.info("Adjusting scales")
    fgip_plot = kwargs.get("fgip_plot", True)
    if fgip_plot:
        contour_plot_axis.set_yscale("fgipscale")
        contour_plot_axis.set_xscale("fgipscale")
    LOGGER.debug("y scale: %s", contour_plot_axis.get_yscale())
    LOGGER.debug("x scale: %s", contour_plot_axis.get_xscale())
    output_filename = create_output_filename(input_data, fileformat)
    plt.savefig(output_filename, format=fileformat, transparent=True)
    plt.close(contour_plot)
    return 0


def plot_pcolormesh_to_contour_map(input_data, contour_plot_axis):
    """This plots a colour mesh on the given axis.
    """
    cmap = cm.get_cmap("RdBu_r")
    contour_plot_axis.pcolormesh(
        input_data["x_data"],
        input_data["y_data"],
        input_data["z_data"],
        cmap=cmap,
        alpha=0.75,
        vmin=input_data.get("levels", [-100.0])[0],
        vmax=input_data.get("levels", [100.0])[-1],
    )


def add_hlines_at_interval(contour_plot_axis, y_value_list, color):
    """This adds lines at all y values supplied.
    """
    for y_value in y_value_list:
        add_hline(contour_plot_axis, y_value, color)


def add_hline(contour_plot_axis, y_value, color):
    """This adds a horizontral line to the given plot.
    """
    contour_plot_axis.axhline(y=y_value, color=color, linewidth=0.25)


def add_vlines_at_interval(contour_plot_axis, x_value_list, color):
    """This adds lines at all x values supplied.
    """
    for x_value in x_value_list:
        add_vline(contour_plot_axis, x_value, color)


def add_vline(contour_plot_axis, x_value, color):
    """This adds a vertical line to the given plot.
    """
    contour_plot_axis.axvline(x=x_value, color=color, linewidth=0.25)


def create_contour_plot(input_data):
    """This creates a contour plot.
    """
    cmap = cm.get_cmap("bwr")
    contour_plot, contour_plot_axis = create_plot_with_axis(
        FIGURE_SIZES["contour plot"]
    )

    add_hlines_at_interval(contour_plot_axis, input_data["h_lines"], color="0.4")
    add_vlines_at_interval(contour_plot_axis, input_data["v_lines"], color="0.4")
    if input_data.get("levels", None) is not None:
        contour_set = contour_plot_axis.contour(
            input_data["x_data"],
            input_data["y_data"],
            input_data["z_data"],
            levels=input_data["levels"],
            colors=["b" if x < 0.0 else "r" for x in input_data["levels"]],
        )
        contour_plot_axis.contourf(
            input_data["x_data"],
            input_data["y_data"],
            input_data["z_data"],
            levels=input_data["levels"],
            cmap=cmap,
            vmin=-20.0,
            vmax=20.0,
            alpha=0.7,
        )
    else:
        contour_set = contour_plot_axis.contour(
            input_data["x_data"], input_data["y_data"], input_data["z_data"], cmap=cmap
        )
    # contour_plot.canvas.draw()
    add_labels_to_contour_plot(contour_plot_axis, contour_set, input_data)
    return contour_plot, contour_plot_axis, contour_set


def add_labels_to_contour_plot(contour_plot_axis, contour_set, input_data):
    """This adds the labels to the contour plot, and formats the axes.
    """
    contour_plot_axis.axis(input_data["plot_axis_range"])
    contour_plot_axis.set_xlabel(input_data["x_label"], fontsize=30)
    contour_plot_axis.set_ylabel(
        input_data["y_label"], fontsize=30, rotation="horizontal"
    )
    contour_plot_axis.yaxis.set_label_coords(-0.1,0.5)
    contour_plot_axis.tick_params(axis="both", labelsize=24)
    contour_set.levels = ["{:+d}".format(x) for x in input_data["levels"]]
    contour_plot_axis.clabel(contour_set, contour_set.levels, fontsize=20, inline=1)


def create_scatter_plot_multidata(
    input_data_list,
    outputfile_stem,
    axis_info,
    fileformat="svg",
    **kwargs
):
    """Creates a scatter plot with mutliple data sets super imposed.

    Parameters
    ---------
    input_data_list
    outputfile_stem
    axis_info
    fileformat
    kwargs

    Returns
    -------
    None
        Output scatter plot is saved to file.
    """
    scatter_plot, scatter_plot_axis = plot_scatter_graph(
        input_data_list[0], label_plot=False, **kwargs,
    )
    for input_data in input_data_list[1:]:
        plot_scatter_graph(
            input_data,
            scatter_plot=scatter_plot,
            scatter_plot_axis=scatter_plot_axis,
            label_plot=False, **kwargs,
        )
    # TODO label plot and axis range.
    add_labels_to_scatter_plot(scatter_plot_axis, axis_info)
    output_filename = create_output_filename(
        {"figure_label": outputfile_stem}, fileformat
    )
    plt.savefig(output_filename, format=fileformat)
    plt.close(scatter_plot)


def createScatterPlot(input_data, fileformat="svg", plot_line=False):
    """function takes input_data and returns a scatter plot of the data
    """
    scatter_plot, scatter_plot_axis = plot_scatter_graph(input_data)
    if plot_line:
        plot_linear_line(scatter_plot_axis, input_data)
    output_filename = create_output_filename(input_data, fileformat)
    plt.savefig(output_filename, format=fileformat)
    plt.close(scatter_plot)
    return 0


def create_scatter_graph_with_polynomial(input_data, fileformat="eps"):
    """This outputs takes input data and plots the graph with the given
    polynomial plotted.
    """
    scatter_plot, scatter_plot_axis = plot_scatter_graph(input_data)
    plot_polynomial(scatter_plot_axis, input_data)
    output_filename = create_output_filename(input_data, fileformat)
    plt.savefig(output_filename, format=fileformat)
    plt.close(scatter_plot)
    return 0


def plot_scatter_graph(
    input_data, scatter_plot=None, scatter_plot_axis=None, label_plot=True
):
    """This plots the data, and labels the axes, with the information given.
    """
    if scatter_plot is None and scatter_plot_axis is None:
        scatter_plot, scatter_plot_axis = create_plot_with_axis(
            FIGURE_SIZES["scatter plots"]
        )
    elif scatter_plot is not None and scatter_plot_axis is None:
        scatter_plot_axis = create_axis(scatter_plot)
    scatter_plot_axis.errorbar(
        input_data["x_data"],
        input_data["y_data"],
        yerr=input_data.get("y_error",None),
        capsize=3.0,
        marker=input_data.get("marker", "+"),
        mec=input_data.get("mec", "b"),
        mfc=input_data.get("mfc", "none"),
        linestyle=input_data.get("linestyle", "None"),
        color=input_data.get("linecolor", "k"),
    )
    if label_plot:
        add_labels_to_scatter_plot(scatter_plot_axis, input_data)
    return scatter_plot, scatter_plot_axis


def plot_polynomial(scatter_plot_axis, input_data):
    """This plots the polynomial curve.
    """
    ordered_x_values = np.sort(input_data["x_data"])
    polynomial_values = generate_polynomial_values(
        ordered_x_values, input_data["polynomial coefficients"]
    )
    plot_polynomial_curve(
        scatter_plot_axis,
        ordered_x_values,
        polynomial_values,
        input_data["polynomial order"],
    )


def plot_polynomial_curve(
    scatter_plot_axis, x_data, polynomial_values, polynomial_order, **kwargs
):
    """This plots the polynomial for the data.
    """
    poly_curve = Line2D(
        x_data,
        polynomial_values,
        linestyle="-",
        linewidth=1.0,
        color=kwargs.pop("linecolour", "r"),
        label=r"y=O(x^{:d})".format(polynomial_order),
    )
    scatter_plot_axis.add_line(poly_curve)


def generate_polynomial_values(x_data, polynomial_coefficients):
    """This generates the polynomial coefficients required for plotting the curve.
    """
    return poly.polyval(x_data, polynomial_coefficients)


def createScatterPlotRings(input_data, fileformat="eps", plot_line=False):
    """function takes input_data dictionary and returns scatter plot of the
    data, with PLOT_SHAPES_FOR_RING_SIZE used for colour and shape of markers.
    """
    scatter_plot, scatter_plot_axis = create_plot_with_axis(
        FIGURE_SIZES["scatter plots"]
    )
    for num_rings in input_data["data_by_num_rings"].keys():
        data_for_plotting = input_data["data_by_num_rings"][num_rings]
        exp_values = data_for_plotting["x_data"]
        calc_values = data_for_plotting["y_data"]
        scatter_plot_axis.plot(
            exp_values,
            calc_values,
            marker=PLOT_SHAPES_FOR_RING_SIZE[num_rings][0],
            mec=PLOT_SHAPES_FOR_RING_SIZE[num_rings][1],
            mfc=PLOT_SHAPES_FOR_RING_SIZE[num_rings][2],
            linestyle="None",
        )
    if plot_line:
        plot_linear_line(scatter_plot_axis, input_data)
    add_labels_to_scatter_plot(scatter_plot_axis, input_data)
    output_filename = create_output_filename(input_data, fileformat)
    plt.savefig(output_filename, format=fileformat)
    plt.close(scatter_plot)
    return 0


def createScatterPlotFunc(input_data, fileformat="eps", plot_line=False):
    """function takes input_data dictionary and returns scatter plot of the
    data, with PLOT_SHAPES_FOR_RING_SIZE used for colour and shape of markers.
    """
    scatter_plot, scatter_plot_axis = create_plot_with_axis(
        FIGURE_SIZES["scatter plots"]
    )
    for func_group in input_data["data_by_func_group"].keys():
        if func_group in PLOT_SHAPES_FOR_FUNC_GROUP.keys():
            data_for_plotting = input_data["data_by_func_group"][func_group]
            exp_values = data_for_plotting["x_data"]
            calc_values = data_for_plotting["y_data"]
            scatter_plot_axis.plot(
                exp_values,
                calc_values,
                marker=PLOT_SHAPES_FOR_FUNC_GROUP[func_group][0],
                mec=PLOT_SHAPES_FOR_FUNC_GROUP[func_group][1],
                mfc=PLOT_SHAPES_FOR_FUNC_GROUP[func_group][2],
                linestyle="None",
            )
    if plot_line:
        plot_linear_line(scatter_plot_axis, input_data)
    add_labels_to_scatter_plot(scatter_plot_axis, input_data)
    output_filename = create_output_filename(input_data, fileformat)
    plt.savefig(output_filename, format=fileformat)
    plt.close(scatter_plot)
    return 0


def add_labels_to_scatter_plot(scatter_plot_axis, input_data, **kwargs):
    """This adds the labels to the scattter plot, and formats the axes.
    """
    scatter_plot_axis.axis(input_data["plot_axis_range"])
    scatter_plot_axis.set_xlabel(input_data["x_label"], fontsize=kwargs.get("label_size",16))
    scatter_plot_axis.set_ylabel(input_data["y_label"], fontsize=kwargs.get("label_size",16))
    scatter_plot_axis.tick_params(labelsize=kwargs.get("tick_label_size",16))
    scatter_plot_axis.set_xscale(input_data.get("x_scale", "linear"))
    scatter_plot_axis.set_yscale(input_data.get("y_scale", "linear"))


def plot_linear_line(scatter_plot_axis, input_data):
    """This plots a linear y=x line on the plot.
    """
    coords = (
        max(input_data["x_axis_range"][0], input_data["y_axis_range"][0]),
        min(input_data["x_axis_range"][1], input_data["y_axis_range"][1]),
    )
    scatter_plot_axis.plot(
        coords, coords, color="k", linestyle="-", linewidth=1, label="y=x"
    )


def createBarPlotRings(input_data, fileformat="eps"):
    """function takes input_data dictionary and returns bar plot of the RMSE of
    the data, by number of rings
    """
    bar_plot, bar_plot_axis = create_bar_plot_for_rmse_values(
        input_data, FIGURE_SIZES["vertical bar chart"], "vertical"
    )
    bar_plot_axis.set_xlabel(r"Number of rings in molecule", fontsize=16)
    output_filename = create_output_filename(input_data, fileformat)
    plt.savefig(output_filename, format=fileformat)
    plt.close(bar_plot)
    return 0


def createBarPlotFuncGroup(input_data, fileformat="eps"):
    """function takes input_data dictionary and returns bar plot of the RMSE of
    the data, by functional group.
    """
    bar_plot, bar_plot_axis = create_bar_plot_for_rmse_values(
        input_data, FIGURE_SIZES["horizontal bar chart"], "horizontal"
    )
    bar_plot_axis.set_ylabel(r"Functional group in molecule", fontsize=16)
    bar_plot_axis.margins(0.1)
    bar_plot.subplots_adjust(left=0.3)
    output_filename = create_output_filename(input_data, fileformat)
    plt.savefig(output_filename, format=fileformat)
    plt.close(bar_plot)
    return 0


def create_bar_plot_for_rmse_values(input_data, figsize, orientation):
    """This creates a bar plot, and returns the plot and axis.
    """
    bar_plot, bar_plot_axis = create_plot_with_axis(figsize)
    rmse_range = np.arange(len(input_data["rmse_values"]))
    rmse_labels = input_data["rmse_values"].keys()
    rmse_values = get_rmse_values(input_data)
    if orientation == "horizontal":
        bar_plot_axis.barh(rmse_range, rmse_values, align="center")
        bar_plot_axis.set_yticks(rmse_range)
        bar_plot_axis.set_yticklabels(rmse_labels)
        bar_plot_axis.set_xlabel(rmse_axis_label(input_data), fontsize=16)
    elif orientation == "vertical":
        bar_plot_axis.bar(rmse_range, rmse_values, align="center")
        bar_plot_axis.set_xticks(input_data["rmse_values"].keys())
        bar_plot_axis.set_ylabel(rmse_axis_label(input_data), fontsize=16)
    return bar_plot, bar_plot_axis


def create_output_filename(input_data, fileformat):
    """This creates the output filename, based on the input data figure label,
    and the fileformat.
    """
    return create_output_filename_from_stem(input_data["figure_label"], fileformat)


def create_output_filename_from_stem(filename_stem, fileformat):
    """This creates the ouput filename.
    """
    return filename_stem + "." + fileformat


def rmse_axis_label(input_data):
    """This returns the label for the axis of the bar plot with RMSE on.
    """
    return r"$RMSE" + input_data["unit"]["x"].unit_label() + "$"


def get_rmse_values(input_data):
    """This gets the RMSE values into a list, ready for plotting as a bar plot.
    """
    rmse_values = []
    for key in input_data["rmse_values"].keys():
        rmse_values.append(input_data["rmse_values"][key])
    return np.array(rmse_values)


def create_plot_with_axis(figsize):
    """Function creates plot and plot axis.
    """
    plot = create_plot(figsize)
    return plot, create_axis(plot)


def create_axis(plot):
    """Function adds subplot to the given plot.
    """
    return plot.add_subplot(111)


def create_plot(figsize):
    """Function creates a figure. Size is specified by given figsize.
    """
    return plt.figure(figsize=figsize)
