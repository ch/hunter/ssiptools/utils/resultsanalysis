#    resultsanalysis provides generic tools and classes for analysis of calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    resultsanalysis is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""module for writing results to file
"""

import csv


def writeRegressionDataToFile(input_data):
    """function outputs csv file for regression analysis of dataset
    """
    with open(input_data["filename"], "w") as outfile:
        csv_writer = csv.writer(outfile, delimiter="\t", quoting=csv.QUOTE_NONE,lineterminator="\n")
        # write header line
        header_line_one = [input_data["value_type"], input_data["unit"]]
        header_line_two = ["slope", "intercept", "R^2", "RMSE"]
        csv_writer.writerow(header_line_one)
        csv_writer.writerow(header_line_two)
        csv_writer.writerow(input_data["Regress_values"])
    return 0


def writeRegressionDataToFileRings(input_data):
    """function outputs csv file for regression analysis of dataset by number
    of rings.
    """
    with open(input_data["filename"], "w") as outfile:
        csv_writer = csv.writer(outfile, delimiter="\t", quoting=csv.QUOTE_NONE,lineterminator="\n")
        # write header line
        header_line_one = [input_data["value_type"], input_data["unit"]]
        header_line_two = ["Number of Rings", "slope", "intercept", "R^2", "RMSE"]
        csv_writer.writerow(header_line_one)
        csv_writer.writerow(header_line_two)
        for num_rings, regress_vals in input_data["Regress_values"].items():
            csv_writer.writerow([num_rings] + list(regress_vals))
    return 0


def writeRegressionDataToFileFuncGroup(input_data):
    """function outputs csv file for regression analysis of dataset by
    functional group.
    """
    with open(input_data["filename"], "w") as outfile:
        csv_writer = csv.writer(outfile, delimiter="\t", quoting=csv.QUOTE_NONE,lineterminator="\n")
        # write header line
        header_line_one = [input_data["value_type"], input_data["unit"]]
        header_line_two = ["Number of Rings", "slope", "intercept", "R^2", "RMSE"]
        csv_writer.writerow(header_line_one)
        csv_writer.writerow(header_line_two)
        func_group_list = sorted(list(input_data["Regress_values"].keys()))
        for func_group in func_group_list:
            regress_vals = input_data["Regress_values"][func_group]
            csv_writer.writerow([func_group] + list(regress_vals))
    return 0


def write_poly_fit_information_to_file(
    poly_fit_info_dict, filename, split_fit_range=False
):
    """This takes the information and outputs to file.
    """
    with open(filename, "w") as outfile:
        csv_writer = csv.writer(outfile, delimiter="\t", quoting=csv.QUOTE_NONE,lineterminator="\n")
        header_line = ["Order", "Fit Range", "RMSE", "Covariance", "Coefficients"]
        csv_writer.writerow(header_line)
        if split_fit_range:
            for polynomial_order in sorted(poly_fit_info_dict.keys()):
                csv_writer.writerow(
                    create_polynomial_fit_info_line(
                        poly_fit_info_dict[polynomial_order]["positive"],
                        fit_range="positive",
                    )
                )
                csv_writer.writerow(
                    create_polynomial_fit_info_line(
                        poly_fit_info_dict[polynomial_order]["negative"],
                        fit_range="negative",
                    )
                )
        else:
            for polynomial_order in sorted(poly_fit_info_dict.keys()):
                csv_writer.writerow(
                    create_polynomial_fit_info_line(
                        poly_fit_info_dict[polynomial_order]
                    )
                )
    return 0


def create_polynomial_fit_info_line(poly_fit_dict, fit_range="All"):
    """This takes a polynomial fit dictionary
    """
    info_line_list = [
        poly_fit_dict["order"],
        fit_range,
        "{:.10f}".format(poly_fit_dict["RMSE"]),
        *["{:.12E}".format(x) for x in poly_fit_dict["covar"].tolist()],
        *["{:.12E}".format(x) for x in poly_fit_dict["coefficients"].tolist()],
    ]

    return info_line_list
