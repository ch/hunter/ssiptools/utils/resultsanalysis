#    resultsanalysis provides generic tools and classes for analysis of calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    resultsanalysis is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""Script containing custom scale for FGIPs, based on:

https://matplotlib.org/gallery/scales/custom_scale.html

@author: mark
"""

import logging
import numpy as np
from matplotlib import scale as mscale
from matplotlib import transforms as mtransforms
from matplotlib.ticker import Formatter, FixedLocator
from matplotlib import rcParams

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)

rcParams["axes.axisbelow"] = False


class FGIPScale(mscale.ScaleBase):
    """Scale class for FGIP.
    """

    name = "fgipscale"

    def __init__(self, axis, **kwargs):
        """
        Any keyword arguments passed to ``set_xscale`` and
        ``set_yscale`` will be passed along to the scale's
        constructor.

        thresh: The value above which to crop the data.
        """
        mscale.ScaleBase.__init__(self, axis)
        thresh = kwargs.pop("thresh", 20.0)
        self.thresh = thresh

    def get_transform(self):
        """Override this method to return a new instance that does the
        actual transformation of the data.

        The MercatorLatitudeTransform class is defined below as a
        nested class of this one.
        """
        return self.FGIPTransform(self.thresh)

    def set_default_locators_and_formatters(self, axis):
        """Override to set up the locators and formatters to use with the
        scale.  This is only required if the scale requires custom
        locators and formatters.  Writing custom locators and
        formatters is rather outside the scope of this example, but
        there are many helpful examples in ``ticker.py``.

        In our case, the Mercator example uses a fixed locator from
        -90 to 90 degrees and a custom formatter class to put convert
        the radians to degrees and put a degree symbol after the
        value:
        """

        class FGIPFormatter(Formatter):
            """Formatter class.
            """
            def __call__(self, x, pos=None):
                """return absolute integer values.
                """
                return "{:.0f}".format(abs(x))

        axis.set_major_locator(
            FixedLocator(
                np.concatenate(
                    (np.arange(-20.0, 1.0, 2.0), np.arange(1.0, 20.0, 1.0)), axis=0
                )
            )
        )
        axis.set_major_formatter(FGIPFormatter())
        axis.set_minor_formatter(FGIPFormatter())

    def limit_range_for_scale(self, vmin, vmax, minpos):
        """Override to limit the bounds of the axis to the domain of the
        transform.  In the case of Mercator, the bounds should be
        limited to the threshold that was passed in.  Unlike the
        autoscaling provided by the tick locators, this range limiting
        will always be adhered to, whether the axis range is set
        manually, determined automatically or changed through panning
        and zooming.
        """
        return max(vmin, -self.thresh), min(vmax, self.thresh)

    class FGIPTransform(mtransforms.Transform):
        """Transform class for FGIP
        """

        input_dims = 1
        output_dims = 1
        is_separable = True
        has_inverse = True

        def __init__(self, thresh):
            mtransforms.Transform.__init__(self)
            self.thresh = thresh

        def transform_non_affine(self, a):
            """This transform takes an Nx1 ``numpy`` array and returns a
            transformed copy.  Since the range of the Mercator scale
            is limited by the user-specified threshold, the input
            array must be masked to contain only valid values.
            ``matplotlib`` will handle masked arrays and remove the
            out-of-range data from the plot.  Importantly, the
            ``transform`` method *must* return an array that is the
            same shape as the input array, since these values need to
            remain synchronized with values in the other dimension.
            """
            LOGGER.debug("input array:")
            LOGGER.debug(a)
            transformed_array = np.array(
                [2.0 * a[i] if a[i] > 0.0 else a[i] for i in range(len(a))]
            )
            LOGGER.debug("transformed array:")
            LOGGER.debug(transformed_array)
            return transformed_array

        def inverted(self):
            """Override this method so matplotlib knows how to get the
            inverse transform for this transform.
            """
            return FGIPScale.InvertedFGIPTransform(self.thresh)

    class InvertedFGIPTransform(mtransforms.Transform):
        """Inverted transform for FGIPs.
        """
        input_dims = 1
        output_dims = 1
        is_separable = True
        has_inverse = True

        def __init__(self, thresh):
            mtransforms.Transform.__init__(self)
            self.thresh = thresh

        def transform_non_affine(self, a):
            return np.array([a[i] / 2.0 if a[i] > 0.0 else a[i] for i in range(len(a))])

        def inverted(self):
            return FGIPScale.FGIPTransform(self.thresh)


# Now that the Scale class has been defined, it must be registered so
# that ``matplotlib`` can find it.
mscale.register_scale(FGIPScale)
