#    resultsanalysis provides generic tools and classes for analysis of calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    resultsanalysis is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""TextFileReader contains functions for reading in csv files containing data.
"""
import copy
import logging
import re
import csv
import resultsanalysis.dataclasses.units as Units
from resultsanalysis.dataclasses.valuetype import ValueType
from resultsanalysis.dataclasses.value import Value
from resultsanalysis.dataclasses.datapoint import Datapoint
from resultsanalysis.dataclasses.molecule import Molecule
from resultsanalysis.dataclasses.molecules import Molecules

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


def readCSVDataFile(filename, delimiter):
    """Read a csv file in after delimiter has been specificed.
    This returns a list, with each element containing the corresponding
    row from the input filename as a list, split using the delimiter.
    Returns a list containing the rows of the file.
    """
    data_in = []
    with open(filename, "r") as file_in:
        file_read = csv.reader(file_in, delimiter=delimiter)
        for line in file_read:
            data_in.append(line)
    return data_in


def parseUnit(unit_string):
    """Function takes a string for a unit and then returns a Units enum
    corresponding to it.
    """
    unit_patterns = {
        "dimensionless": re.compile(""),
        "atomic_units_potential": re.compile("(a.u.Potential)|(A.U.Potential)"),
        "volts": re.compile("(V)|(Volts)"),
        "atomic_units_energy": re.compile("(a.u.Energy)|(A.U.Energy)"),
        "kj_per_mol": re.compile("(kJ/mol)|(KJmol-1)|(kJmol-1)|(kjmol-1)"),
        "kcal_per_mol": re.compile("(kcal/mol)|(Kcalmol-1)|(kcalmol-1)"),
    }
    for unit, pattern in unit_patterns.items():
        if pattern.match(unit_string):
            unit_label = unit
    return Units.createUnitsFromString(unit_label)


def readCSVHeaders(headers):
    """Function used to parse headers. This gets the order of columns
    in the csv file.
    A dictionary is returned containing the column index of values.
    Experimental and calculated values are grouped along with unit,
    the valueType is the key.

    headers = ['Name', 'InChIKey', 'Log(Sw)exp',
    'Smiles Canonicalised', 'Log(Sw)calc']
    should return:
    locationdict= {'InChIKey': 1,
    'Log(Sw)': {'Calc': 4, 'Exp': 2, 'Unit': ''},
    'Name': 0,
    'SMILES': 3}
    """
    LOGGER.info("reading headers")
    patterns = {
        "Name": re.compile("(Name)|(name)|(NAME)"),
        "InChIKey": re.compile("(InChIKey)|(inchikey)|(INCHIKEY)"),
        "SMILES": re.compile("(SMILES)|(Smiles)|(smiles)"),
        "Exp": re.compile("(exp)|(Exp)|(EXP)"),
        "Calc": re.compile("(calc)|(Calc)|(CALC)"),
    }
    location_dict = {}
    exp_values = []
    calc_values = []
    LOGGER.debug("starting iteration over headers")
    for idx, header in enumerate(headers):
        LOGGER.debug("Header: " + header + " index: " + str(idx))
        if patterns["Name"].match(header):
            LOGGER.debug("Name column found")
            location_dict["Name"] = idx
        elif patterns["InChIKey"].match(header):
            LOGGER.debug("")
            location_dict["InChIKey"] = idx
        elif patterns["SMILES"].match(header):
            location_dict["SMILES"] = idx
        elif patterns["Exp"].search(header):
            exp_info = re.split(patterns["Exp"], header)
            exp_info = [e for e in exp_info if e is not None]
            exp_info.append(idx)
            exp_values.append(exp_info)
        elif patterns["Calc"].search(header):
            calc_info = re.split(patterns["Calc"], header)
            calc_info = [c for c in calc_info if c is not None]
            calc_info.append(idx)
            calc_values.append(calc_info)
    for e_value in exp_values:
        for c_value in calc_values:
            location_dict[(e_value[0], c_value[0])] = {
                "Exp": e_value[-1],
                "Calc": c_value[-1],
                "Unit": {"x": parseUnit(e_value[-2]), "y": parseUnit(c_value[-2])},
            }
    return location_dict


def valueTypes(header_locations):
    """function for getting the Types of values from the file
    """
    value_types = []
    for key in header_locations:
        if key != "Name" and key != "InChIKey" and key != "SMILES":
            value_types.append(key)
    return value_types


def createDatapoint(value_type, header_locations, data):
    """createDatapoint creates a datapoint object from read in data,
    for a specific value_type, using the header_locations to correctly assign
    datapoint attributes.
    """
    if (
        data[header_locations[value_type]["Exp"]] != ""
        and data[header_locations[value_type]["Calc"]] != ""
    ):
        exp_value = Value(
            float(data[header_locations[value_type]["Exp"]]),
            value_type[0],
            ValueType.experimental,
            header_locations[value_type]["Unit"]["x"],
        )
        calc_value = Value(
            float(data[header_locations[value_type]["Calc"]]),
            value_type[1],
            ValueType.calculated,
            header_locations[value_type]["Unit"]["y"],
        )
        LOGGER.debug("Experimental Value:")
        LOGGER.debug(exp_value)
        LOGGER.debug("Calculated Value:")
        LOGGER.debug(calc_value)
        data_values = [exp_value, calc_value, data[header_locations["InChIKey"]]]
        datapoint = Datapoint.create_datapoint(data_values)
        LOGGER.debug("Created Datapoint:")
        LOGGER.debug(datapoint)
        return datapoint


def createDatapointList(value_types, header_locations, data):
    """createDatapointList creates Datapoint objects for all pairs of
    calculated and experimental data in the row.
    """
    datapoints = []
    for value_type in value_types:
        datapoint = createDatapoint(value_type, header_locations, data)
        if datapoint:
            datapoints.append(datapoint)
    return datapoints


def createMolecule(header_locations, data, datapoints):
    """createMolecule creates a Molecule object from the read in data.
    """
    data_values = [
        data[header_locations["InChIKey"]],
        data[header_locations["Name"]],
        data[header_locations["SMILES"]],
    ]
    molecule = Molecule.create_molecule_with_datapoints(data_values, datapoints)
    return molecule


def createMoleculeList(header_locations, data):
    """createMoleculeList takes header_locations and data
    """
    molecules_list = []
    value_types = valueTypes(header_locations)
    for datum in data:
        datapoints_list = createDatapointList(value_types, header_locations, datum)
        molecule = createMolecule(header_locations, datum, datapoints_list)
        molecules_list.append(molecule)
    return molecules_list


def createMolecules(header_locations, molecule_list):
    """returns a Molecules object with the datapoints_list populated with data.
    """
    molecules = Molecules()
    molecules.addMoleculeList(molecule_list)
    return molecules


def parseCSVDataFile(filename, delimiter):
    """This function creates a molecules object, from a read in csv file,
    filename. The delimiter is used to split the rows into the columns.
    """
    data_in = readCSVDataFile(filename, delimiter)
    headers = data_in[0]
    header_locations = readCSVHeaders(headers)
    data_list = copy.deepcopy(data_in[1:])
    molecule_list = createMoleculeList(header_locations, data_list)
    molecules = createMolecules(header_locations, molecule_list)
    return molecules
