#    resultsanalysis provides generic tools and classes for analysis of calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    resultsanalysis is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Script for extracting smarts strings from a file
"""

from rdkit import Chem
import logging

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class SmartsFragmentsList(object):
    """Class to store the SMARTS strings for fragment and functional group
    searching in the molecules in test sets.
    """

    def __init__(self):
        """create instance of class
        """
        self.smarts_strings = {}
        self.smarts_mol_form = {}

    def __str__(self):
        """return string with number of SMARTS strings in moleucle
        """
        to_string = "SmartsFragmentsList with %d smarts substructures." % (
            len(self.smarts_strings)
        )
        return to_string

    def __eq__(self, other_frag_list):
        """Overload so a comparison of the smarts_strings is used
        """
        value = True
        for key, value in self.smarts_strings.items():
            if key not in other_frag_list.smarts_strings.keys():
                return False
            if value != other_frag_list.smarts_strings[key]:
                return False
        return value

    def addSmartsString(self, smarts_name, smarts_string):
        """Add smarts string to list- smarts_name is key for the dictionary
        entry. It is expressed as a string in self.smarts_strings, and as a
        rdkit.Chem Mol object in self.smarts_mol_form
        """
        self.smarts_strings[smarts_name] = smarts_string
        self.smarts_mol_form[smarts_name] = Chem.MolFromSmarts(smarts_string)

    def addSmartsStringList(self, smarts_list):
        """Adds a list of smarts strings, which are stored as tuples or lists
        in smarts_list, with smarts_list[i][0] = smarts_name,
        smarts_list[i][1] = smarts_string
        """
        for i in range(len(smarts_list)):
            self.addSmartsString(smarts_list[i][0], smarts_list[i][1])


def readSmartsFromFile(filename):
    """readSmartsFromFile reads in a text file where the name of the
    functional group and corresponding SMARTS pattern are split by ': '
    Improvements needed?
    """
    with open(filename, "r") as smarts_file:
        smarts_list_from_file = smarts_file.readlines()
        LOGGER.info("File has been read into list")
        for i in range(len(smarts_list_from_file)):
            smarts_list_from_file[i] = smarts_list_from_file[i].strip()
            smarts_list_from_file[i] = smarts_list_from_file[i].split(": ")
            smarts_list_from_file[i] = tuple(smarts_list_from_file[i])
    return smarts_list_from_file
