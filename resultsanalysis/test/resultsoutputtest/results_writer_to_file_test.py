#    resultsanalysis provides generic tools and classes for analysis of calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    resultsanalysis is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Contains test case for the ResultsWriterToFile script.

@author: mark
"""

import unittest
import logging
import os
import pathlib
import numpy as np
import resultsanalysis.resultsoutput.resultswritertofile as ResultsWriterToFile

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class ResultsWriterToFileTestCase(unittest.TestCase):
    """Test case for the Datapoint object.
    """

    def setUp(self):
        """Set up for tests
        """
        self.maxDiff = None
        self.parent_directory = pathlib.Path(__file__).parents[1]

        self.regress_data = {
            "value_type": r"Log(Sw)",
            "unit": "",
            "filename": "Log(Sw)Regress.csv",
            "Regress_values": [
                0.94580475768996797,
                -0.040624497087759392,
                0.84054491317976587,
                0.73156747724549709,
            ],
        }
        self.regress_data_rings = {
            "value_type": r"Log(Sw)",
            "unit": "",
            "filename": "Log(Sw)RegressByRings.csv",
            "Regress_values": {
                0: [
                    0.97690288027046623,
                    -0.012309754154302022,
                    0.86660370885618132,
                    0.69957384311305337,
                ],
                1: [
                    0.83524680338136081,
                    -0.17909561287292597,
                    0.7312028475849035,
                    0.82222027265498321,
                ],
                2: [
                    0.92673850681663061,
                    -0.26170595938753349,
                    0.7509627399987272,
                    0.55943719455670482,
                ],
                3: [None, None, None, 0.044162779199999891],
            },
        }
        self.regress_data_func = {
            "value_type": r"Log(Sw)",
            "unit": "",
            "filename": "Log(Sw)RegressByFunc.csv",
            "Regress_values": {
                "Alkyl_Bromo": [
                    1.1296586175807268,
                    0.50979157392773855,
                    0.96478211326115237,
                    0.38005339216713757,
                ],
                "Phenol": [
                    1.4439728297344108,
                    0.24564290517748244,
                    0.99993553661173729,
                    0.21547197346406904,
                ],
                "Thiophene": [None, None, None, 0.0020971647999999954],
                "Aryl_Iodo": [None, None, None, 0.41706447430000004],
                "Carbonate": [
                    1.4217396499999999,
                    0.52268706330000003,
                    1.0,
                    0.44879051200944287,
                ],
                "Tertiary_aliph_amine": [
                    1.3722093940536066,
                    1.4024018626219941,
                    0.99798406957319685,
                    1.8879327062087594,
                ],
                "Aryl_Fluoro": [None, None, None, 0.13110512569999999],
                "Aldehyde": [
                    0.53649188760344824,
                    0.16500460601206898,
                    0.98357092515535038,
                    0.67166734663221972,
                ],
                "Ester": [
                    0.95986351659685565,
                    0.27029736376738911,
                    0.64271458562470518,
                    0.65782402079037339,
                ],
                "Tertiary_alcohol": [
                    0.79785233057142868,
                    -0.28527725621428568,
                    1.0,
                    0.26459007314103089,
                ],
                "Alkene": [
                    1.1941109010468236,
                    0.65188451435545636,
                    0.95135441651372554,
                    0.43063797317481578,
                ],
                "Nitro": [
                    0.52878347474435716,
                    0.092329261864543599,
                    0.8017292639978123,
                    0.99791063819062686,
                ],
                "Aryl_Bromo": [None, None, None, 0.30333518650000002],
                "Alkylarylether": [
                    1.4754466420429446,
                    1.076950358207055,
                    0.88806432802540269,
                    0.33662611993667307,
                ],
                "Phenyl": [
                    1.1189224307508423,
                    0.60070077474367611,
                    0.77848251295217752,
                    0.74654883810356021,
                ],
                "Primary_alcohol": [
                    1.0613934911559657,
                    -0.07702146311865099,
                    0.97178436701877602,
                    0.34117193006412,
                ],
                "Pyridine": [
                    1.0459583072910448,
                    -1.3708985234053068,
                    0.33771237337496224,
                    1.4659648292696708,
                ],
                "Alkyl_Fluoro": [
                    0.40406063539285708,
                    -1.4050944150059528,
                    0.96867514039485247,
                    2.2778283361217082,
                ],
                "Nitrile": [None, None, None, 0.27079177699999996],
                "Aryl_Chloro": [
                    0.97723358394804394,
                    0.41476295839889965,
                    0.80685424236522008,
                    0.65249229506764284,
                ],
                "Napthyl": [
                    1.8079562147469881,
                    3.609852188993977,
                    0.84860987182501824,
                    0.40604889686275014,
                ],
                "Secondary_alcohol": [
                    0.70334000374183991,
                    -0.2158171252109792,
                    0.44617864066851576,
                    0.47090176220611712,
                ],
                "Alkylalkylether": [
                    1.1924778259800406,
                    -0.17122878900760241,
                    0.87779641693529287,
                    0.78074579532544663,
                ],
                "Alkyl_Chloro": [
                    1.0167780747272726,
                    0.1672181764012115,
                    0.90542013591628179,
                    0.29887719205019664,
                ],
                "Primary_aliph_amine": [
                    0.86730037197220911,
                    -0.97934043073506838,
                    0.64135539557400256,
                    1.3034540635898475,
                ],
                "DiSulfide": [None, None, None, 0.36291066059999988],
                "Ketone": [
                    1.0341386634041807,
                    0.34887230568747263,
                    0.87847169418181725,
                    0.52775691142579939,
                ],
                "Phosphate": [None, None, None, 0.069272673199999968],
                "Primary_Aniline": [
                    0.48660341099696514,
                    0.23526550704582699,
                    0.6866630707316862,
                    1.019261276620244,
                ],
                "Quinoline": [None, None, None, 0.19067096890000013],
                "Carboxylic_acid": [
                    0.66253980616281904,
                    -0.45051905648209478,
                    0.88935292888874973,
                    0.79302631218414088,
                ],
                "Alkyl_Iodo": [
                    1.4295162574488189,
                    0.95798750283307132,
                    0.98265236381926435,
                    0.35778578683966322,
                ],
                "Pyrrole": [None, None, None, 1.1707996742],
                "Secondary_aliph_amine": [
                    1.1767217731288455,
                    -1.4662260158354326,
                    0.9777496924163257,
                    1.4553397875377954,
                ],
                "Sulfide": [
                    1.2896346755231922,
                    0.29382019398267323,
                    0.83303353071612896,
                    0.54940042954365698,
                ],
            },
        }

    def tearDown(self):
        """Tear down for tests.
        """
        del self.regress_data
        del self.regress_data_rings
        del self.regress_data_func
        if os.path.isfile("Log(Sw)Regress.csv"):
            os.remove("Log(Sw)Regress.csv")
        if os.path.isfile("Log(Sw)RegressByRings.csv"):
            os.remove("Log(Sw)RegressByRings.csv")
        if os.path.isfile("Log(Sw)RegressByFunc.csv"):
            os.remove("Log(Sw)RegressByFunc.csv")
        if os.path.isfile("actual_poly_fit.csv"):
            os.remove("actual_poly_fit.csv")
        if os.path.isfile("actual_poly_fit2.csv"):
            os.remove("actual_poly_fit2.csv")

    def test_write_reg_data_to_file(self):
        """Test to see if regression data is written to file as expected.
        """
        expected_file_name = (
            (self.parent_directory / "resources/expected_logSwRegress.csv")
            .absolute()
            .as_posix()
        )
        actual_file_name = "Log(Sw)Regress.csv"
        reg_file_out = ResultsWriterToFile.writeRegressionDataToFile(self.regress_data)
        self.assertEqual(reg_file_out, 0)
        with open(expected_file_name, "r") as exp_file:
            exp_file_lines = exp_file.readlines()
            with open(actual_file_name, "r") as act_file:
                act_file_lines = act_file.readlines()
                self.assertListEqual(act_file_lines, exp_file_lines)

    def test_write_reg_data_rings_to_file(self):
        """Test to see if regression data is written to file as expected.
        """
        expected_file_name = (
            (self.parent_directory / "resources/expected_logSwRegressByRings.csv")
            .absolute()
            .as_posix()
        )
        actual_file_name = "Log(Sw)RegressByRings.csv"
        reg_file_out = ResultsWriterToFile.writeRegressionDataToFileRings(
            self.regress_data_rings
        )
        self.assertEqual(reg_file_out, 0)
        with open(expected_file_name, "r") as exp_file:
            exp_file_lines = exp_file.readlines()
            with open(actual_file_name, "r") as act_file:
                act_file_lines = act_file.readlines()
                self.assertListEqual(act_file_lines, exp_file_lines)

    def test_write_reg_data_func_to_file(self):
        """Test to see if expected file is written out.
        """
        expected_file_name = (
            (self.parent_directory / "resources/expected_logSwRegressByFunc.csv")
            .absolute()
            .as_posix()
        )
        actual_file_name = "Log(Sw)RegressByFunc.csv"
        reg_file_out = ResultsWriterToFile.writeRegressionDataToFileFuncGroup(
            self.regress_data_func
        )
        self.assertEqual(reg_file_out, 0)
        with open(expected_file_name, "r") as exp_file:
            exp_file_lines = exp_file.readlines()
            with open(actual_file_name, "r") as act_file:
                act_file_lines = act_file.readlines()
                self.assertListEqual(act_file_lines, exp_file_lines)

    def test_create_polynomial_fit_info_line(self):
        """Test to see if expected list is produced.
        """
        input_dict = {
            "coefficients": np.array([-0.0500, 1.07142857]),
            "RMSE": 0.015430334996209221,
            "order": 1,
            "covar": np.array([0.000714]),
        }
        expected_list = [
            1,
            "All",
            "0.0154303350",
            "7.140000000000E-04",
            "-5.000000000000E-02",
            "1.071428570000E+00",
        ]
        actual_list = ResultsWriterToFile.create_polynomial_fit_info_line(input_dict)
        self.assertEqual(len(expected_list), len(actual_list))
        for i in range(len(expected_list)):
            self.assertEqual(expected_list[i], actual_list[i])

    def test_write_poly_fit_information_to_file(self):
        """Test to see fi expected file is produced.
        """
        expected_file_name = (
            (self.parent_directory / "resources/expected_poly_fit.csv")
            .absolute()
            .as_posix()
        )
        actual_file_name = "actual_poly_fit.csv"
        input_dict = {
            1: {
                "coefficients": np.array([-0.0500, 1.07142857]),
                "RMSE": 0.015430334996209221,
                "order": 1,
                "covar": np.array([0.000714]),
            }
        }
        poly_file_out = ResultsWriterToFile.write_poly_fit_information_to_file(
            input_dict, actual_file_name
        )
        self.assertEqual(0, poly_file_out)
        with open(expected_file_name, "r") as exp_file:
            exp_file_lines = exp_file.read()
            with open(actual_file_name, "r") as act_file:
                act_file_lines = act_file.read()
                self.assertMultiLineEqual(act_file_lines, exp_file_lines)
        expected_file_name2 = (
            (self.parent_directory / "resources/expected_poly_fit2.csv")
            .absolute()
            .as_posix()
        )
        actual_file_name2 = "actual_poly_fit2.csv"
        input_dict2 = {
            1: {
                "positive": {
                    "coefficients": np.array([-0.0500, 1.07142857]),
                    "RMSE": 0.015430334996209221,
                    "order": 1,
                    "covar": np.array([0.000714]),
                },
                "negative": {
                    "coefficients": np.array([-0.0500, 1.07142857]),
                    "RMSE": 0.015430334996209221,
                    "order": 1,
                    "covar": np.array([0.000714]),
                },
            }
        }
        poly_file_out2 = ResultsWriterToFile.write_poly_fit_information_to_file(
            input_dict2, actual_file_name2, split_fit_range=True
        )
        self.assertEqual(0, poly_file_out2)
        with open(expected_file_name2, "r") as exp_file:
            exp_file_lines = exp_file.read()
            with open(actual_file_name2, "r") as act_file:
                act_file_lines = act_file.read()
                self.assertMultiLineEqual(act_file_lines, exp_file_lines)
