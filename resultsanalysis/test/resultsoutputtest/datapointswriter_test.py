#    resultsanalysis provides generic tools and classes for analysis of calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    resultsanalysis is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Created on Sun May 13 12:02:31 2018

@author: mark
"""

import unittest
import logging
import os
import pathlib
from resultsanalysis.dataclasses.datapoint import Datapoint
from resultsanalysis.dataclasses.units import Units
from resultsanalysis.dataclasses.value import Value
from resultsanalysis.dataclasses.valuetype import ValueType
from resultsanalysis.dataclasses.datapoints import Datapoints
import resultsanalysis.resultsoutput.datapointswriter as datapointswriter

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class DatapointsWriterTestCase(unittest.TestCase):
    """Test case for datapoints writer methods.
    """

    def setUp(self):
        """Set up for tests
        """
        self.datapoints_2 = Datapoints.fromdatapoints(
            [
                Datapoint.from_values(
                    Value(1.0, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(1.0, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                    "EXAMPLE1",
                ),
                Datapoint.from_values(
                    Value(2.0, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(2.1, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                    "EXAMPLE2",
                ),
                Datapoint.from_values(
                    Value(0.5, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(0.5, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                    "EXAMPLE3",
                ),
            ]
        )
        parent_directory = pathlib.Path(__file__).parents[1]
        self.expected_filename = (
            (parent_directory / "resources/expecteddatapoints.csv")
            .absolute()
            .as_posix()
        )
        self.actual_filename = "datapoints.csv"

    def tearDown(self):
        """Tear down for tests.
        """
        del self.datapoints_2
        if os.path.isfile(self.actual_filename):
            os.remove(self.actual_filename)

    def test_writedatapoints_to_file(self):
        """Test to see if expected file is written.
        """

        file_out = datapointswriter.writedatapoints_to_file(
            self.datapoints_2, self.actual_filename
        )
        self.assertEqual(file_out, None)
        with open(self.expected_filename, "r") as exp_file:
            exp_file_lines = exp_file.read()
            with open(self.actual_filename, "r") as act_file:
                act_file_lines = act_file.read()
                self.assertEqual(act_file_lines, exp_file_lines)
