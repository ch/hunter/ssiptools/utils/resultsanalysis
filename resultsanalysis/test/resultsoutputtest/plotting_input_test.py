#    resultsanalysis provides generic tools and classes for analysis of calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    resultsanalysis is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Contains test case for the PlottingInput script.

@author: mark
"""
import unittest
import logging
import numpy as np
from resultsanalysis.dataclasses.units import Units
import resultsanalysis.resultsoutput.plottinginput as PlottingInput

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class PlottingInputTestCase(unittest.TestCase):
    """Test case for the Datapoint object.
    """

    def setUp(self):
        """Set up for tests
        """
        self.example_data = {
            "rmse_values": {"test_1": 0.1},
            "unit": {"x": Units.kj_per_mol},
            "figure_label": "test_name",
        }

    def tearDown(self):
        """Tear down for tests.
        """
        del self.example_data

    def test_generate_polynomial_values(self):
        """Test to see if expected values outputted.
        """
        expected_values = np.array([1.0])
        actual_values = PlottingInput.generate_polynomial_values(
            np.array([1.0]), np.array([0.0, 1.0])
        )
        np.testing.assert_array_almost_equal(expected_values, actual_values)

    def test_create_output_filename(self):
        """Test to see if expected pro
        """
        expected_filename = "test_name.eps"
        actual_filename = PlottingInput.create_output_filename(self.example_data, "eps")
        self.assertEqual(expected_filename, actual_filename)

    def test_rmse_axis_label(self):
        """Test to see if expected label produced.
        """
        expected_string = "$RMSE/kJmol^{-1}$"
        actual_string = PlottingInput.rmse_axis_label(self.example_data)
        self.assertEqual(expected_string, actual_string)

    def test_get_rmse_values(self):
        """Test to see if expected array is produced.
        """
        expected_array = np.array([0.1])
        actual_array = PlottingInput.get_rmse_values(self.example_data)
        np.testing.assert_array_almost_equal(expected_array, actual_array)
