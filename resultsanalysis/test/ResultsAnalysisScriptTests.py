#    resultsanalysis provides generic tools and classes for analysis of calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    resultsanalysis is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script for running the tests

@author: mark
"""

import unittest
import sys
import logging
from resultsanalysis.test.dataclassestest.units_test import UnitsTestCase
from resultsanalysis.test.dataclassestest.value_test import ValueTestCase
from resultsanalysis.test.dataclassestest.datapoint_test import DatapointTestCase
from resultsanalysis.test.dataclassestest.datapoints_test import DatapointsTestCase
from resultsanalysis.test.dataclassestest.molecule_test import MoleculeTestCase
from resultsanalysis.test.dataclassestest.molecules_test import MoleculesTestCase
from resultsanalysis.test.fileparsingtest.text_file_reader_test import TextFileReaderTestCase
from resultsanalysis.test.fileparsingtest.smarts_string_sorter_test import SmartsStringSorterTestCase
from resultsanalysis.test.resultsoutputtest.plotting_input_test import PlottingInputTestCase
from resultsanalysis.test.resultsoutputtest.results_writer_to_file_test import ResultsWriterToFileTestCase
from resultsanalysis.test.resultsoutputtest.datapointswriter_test import DatapointsWriterTestCase

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)

DATACLASSESTESTCASES = [
    UnitsTestCase,
    ValueTestCase,
    DatapointTestCase,
    DatapointsTestCase,
    MoleculeTestCase,
    MoleculesTestCase,
]

FILEPARSINGTESTCASES = [TextFileReaderTestCase, SmartsStringSorterTestCase]
RESULTSOUTPUTTESTCASES = [
    PlottingInputTestCase,
    ResultsWriterToFileTestCase,
    DatapointsWriterTestCase,
]


def create_test_suite():
    """Function creates a test suite and then loads all the tests from the
    different test cases.
    """
    LOGGER.info("setting up loader and test suite")
    loader = unittest.TestLoader()
    suite = unittest.TestSuite()
    for test_case in DATACLASSESTESTCASES:
        LOGGER.debug("Adding %s", test_case)
        suite.addTests(loader.loadTestsFromTestCase(test_case))
    for test_case in FILEPARSINGTESTCASES:
        LOGGER.debug("Adding %s", test_case)
        suite.addTests(loader.loadTestsFromTestCase(test_case))
    for test_case in RESULTSOUTPUTTESTCASES:
        LOGGER.debug("Adding %s", test_case)
        suite.addTests(loader.loadTestsFromTestCase(test_case))
    return suite


def run_tests():
    """Runs test suite. Exits if there is a failure.

    Returns
    --------
    None
    """
    LOGGER.info("calling test suite method")
    suite = create_test_suite()
    LOGGER.info("running test suite")
    ret = (
        not unittest.TextTestRunner(verbosity=2, stream=sys.stderr)
        .run(suite)
        .wasSuccessful()
    )
    if ret:
        sys.exit(ret)


if __name__ == "__main__":
    run_tests()
