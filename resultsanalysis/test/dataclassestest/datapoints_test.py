# -*- coding: utf-8 -*-
"""
Script contains tests for Datapoints object.

@author: mark
"""

import unittest
import logging
import numpy as np
from resultsanalysis.dataclasses.datapoint import Datapoint
from resultsanalysis.dataclasses.units import Units
from resultsanalysis.dataclasses.value import Value
from resultsanalysis.dataclasses.valuetype import ValueType
from resultsanalysis.dataclasses.datapoints import Datapoints

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class DatapointsTestCase(unittest.TestCase):
    """Test case for the Datapoint object.
    """

    def setUp(self):
        """Set up for tests
        """
        self.datapoints_1 = Datapoints(None)
        self.datapoints_2 = Datapoints.fromdatapoints(
            [
                Datapoint.from_values(
                    Value(1.0, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(1.0, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                    "EXAMPLE1",
                ),
                Datapoint.from_values(
                    Value(2.0, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(2.1, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                    "EXAMPLE2",
                ),
                Datapoint.from_values(
                    Value(0.5, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(0.5, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                    "EXAMPLE3",
                ),
            ]
        )

    def tearDown(self):
        """Tear down for tests.
        """
        del self.datapoints_1
        del self.datapoints_2

    def test___str__(self):
        """Test to see if expected string is returned.
        """
        expected_string = "Datapoints()"
        actual_string = str(self.datapoints_1)
        self.assertEqual(actual_string, expected_string)

    def test___repr__(self):
        """Test to see if expected repr is returned.
        """
        self.maxDiff = None
        expected_repr_1 = """Datapoints()"""
        actual_repr_1 = repr(self.datapoints_1)
        self.assertEqual(expected_repr_1, actual_repr_1)
        expected_repr_2 = """Datapoints(          Log(Sw)_{Exp}    Log(Sw)_{Exp}Unit  Log(Sw)_{Calc}   Log(Sw)_{Calc}Unit
EXAMPLE1            1.0  Units.dimensionless             1.0  Units.dimensionless
EXAMPLE2            2.0  Units.dimensionless             2.1  Units.dimensionless
EXAMPLE3            0.5  Units.dimensionless             0.5  Units.dimensionless)"""
        actual_repr_2 = repr(self.datapoints_2)
        self.assertEqual(expected_repr_2, actual_repr_2)

    def test___eq__(self):
        """Test to see if datapoints are correctly as equal/not equal.
        """
        expected_datapoints = Datapoints.fromdatapoints(
            [
                Datapoint.from_values(
                    Value(1.0, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(1.0, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                    "EXAMPLE1",
                ),
                Datapoint.from_values(
                    Value(2.0, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(2.1, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                    "EXAMPLE2",
                ),
                Datapoint.from_values(
                    Value(0.5, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(0.5, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                    "EXAMPLE3",
                ),
            ]
        )
        self.assertEqual(self.datapoints_2, expected_datapoints)
        self.assertNotEqual(self.datapoints_1, self.datapoints_2)
        self.datapoints_1.addDatapointList(
            [
                Datapoint.from_values(
                    Value(1.1, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(1.0, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                    "EXAMPLE1",
                ),
                Datapoint.from_values(
                    Value(2.0, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(2.1, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                    "EXAMPLE2",
                ),
                Datapoint.from_values(
                    Value(0.5, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(0.5, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                    "EXAMPLE3",
                ),
            ]
        )
        self.assertNotEqual(self.datapoints_1, expected_datapoints)
        expected_datapoints_2 = Datapoints()
        expected_datapoints_2.addDatapointList(
            [
                Datapoint.from_values(
                    Value(1.0, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(1.0, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                    "EXAMPLE",
                ),
                Datapoint.from_values(
                    Value(2.0, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(2.1, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                    "EXAMPLE2",
                ),
                Datapoint.from_values(
                    Value(0.5, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(0.5, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                    "EXAMPLE3",
                ),
            ]
        )
        self.assertNotEqual(self.datapoints_2, expected_datapoints_2)

    def test_x_values(self):
        """Test to see if expected x values returned.
        """
        expected_x_values = np.array([1.0, 2.0, 0.5])
        actual_array = self.datapoints_2.x_values()
        np.testing.assert_almost_equal(actual_array, expected_x_values)

    def test_y_values(self):
        """Test to see if expected y values returned.
        """
        expected_x_values = np.array([1.0, 2.1, 0.5])
        actual_array = self.datapoints_2.y_values()
        np.testing.assert_almost_equal(actual_array, expected_x_values)

    def test__sorted_datapoints_list(self):
        """Test to see if expected sorted list is returned.
        """
        expected_list = Datapoints.fromdatapoints(
            [
                Datapoint.from_values(
                    Value(1.0, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(1.0, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                    "EXAMPLE1",
                ),
                Datapoint.from_values(
                    Value(2.0, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(2.1, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                    "EXAMPLE2",
                ),
                Datapoint.from_values(
                    Value(0.5, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(0.5, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                    "EXAMPLE3",
                ),
            ]
        )
        actual_list = self.datapoints_2._sorted_datapoints_list()
        self.assertEqual(actual_list, expected_list)

    def test_add_datapoint(self):
        """Test to see if datapoint is added.
        """
        self.datapoints_1.addDatapoint(
            Datapoint.from_values(
                Value(1.0, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                Value(1.0, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                "EXAMPLE1",
            )
        )
        expected_datapoints = Datapoints.fromdatapoints(
            [
                Datapoint.from_values(
                    Value(1.0, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(1.0, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                    "EXAMPLE1",
                )
            ]
        )
        self.assertEqual(self.datapoints_1, expected_datapoints)

    def test_add_datapoint_list(self):
        """Test to see if datapoint is added.
        """
        self.datapoints_1.addDatapointList(
            [
                Datapoint.from_values(
                    Value(1.0, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(1.0, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                    "EXAMPLE1",
                )
            ]
        )
        expected_datapoints = Datapoints.fromdatapoints(
            [
                Datapoint.from_values(
                    Value(1.0, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(1.0, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                    "EXAMPLE1",
                )
            ]
        )
        self.assertEqual(expected_datapoints, self.datapoints_1)

    def test_value_type_tuple(self):
        """Test to see if expected tuple is returned.
        """
        expected_tuple = ("Log(Sw)_{Exp}", "Log(Sw)_{Calc}")
        actual_tuple = self.datapoints_2.value_type_tuple()
        self.assertEqual(actual_tuple, expected_tuple)

    def test_unit_tuple(self):
        """Test to see if expected tuple is returned.
        """
        expected_tuple = (
            ("Log(Sw)_{Exp}Unit", "Log(Sw)_{Calc}Unit"),
            (Units.dimensionless, Units.dimensionless),
        )
        actual_tuple = self.datapoints_2.unit_tuple()
        self.assertEqual(expected_tuple, actual_tuple)

    def test_calc_extreme_vals(self):
        """Test to see if expected extreme values for the calculated values are
        returned.
        """
        expected_values = (0.5, 2.1)
        actual_values = self.datapoints_2.calcDataExtremeValues()
        self.assertEqual(actual_values, expected_values)

    def test_exp_extreme_vals(self):
        """Test to see if expected extreme values for the calculated values are
        returned.
        """
        expected_values = (0.5, 2.0)
        actual_values = self.datapoints_2.expDataExtremeValues()
        self.assertEqual(actual_values, expected_values)

    def test_r_squared(self):
        """Test to see if expected r squared value is returned.
        """
        expected_value = 0.99946695
        actual_value = self.datapoints_2.rSquared()
        self.assertAlmostEqual(actual_value, expected_value)

    def test_rmse(self):
        """Test to see if expected r squared value is returned.
        """
        expected_value = 0.05773503
        actual_value = self.datapoints_2.rmse()
        self.assertAlmostEqual(actual_value, expected_value)

    def test_lin_slope(self):
        """Test to see if expected gradient is returned.
        """
        expected_value = 1.07142857
        actual_value = self.datapoints_2.linearSlope()
        self.assertAlmostEqual(actual_value, expected_value)

    def test_lin_intercept(self):
        """Test to see if expected gradient is returned.
        """
        expected_value = -0.050000000
        actual_value = self.datapoints_2.linearIntercept()
        self.assertAlmostEqual(actual_value, expected_value)

    def test_regress(self):
        """Test to see if expected list is returned for regression analysis
        """
        expected_value = np.array([1.07142857, -0.0500, 0.99946695, 0.05773503])
        actual_value = np.array(self.datapoints_2.regress())
        np.testing.assert_array_almost_equal(actual_value, expected_value)

    def test_polynomial_fit_with_covar(self):
        """Test to see if expected results are returned.
        """
        expected_value_tuple = (np.array([1.025]), np.array([[0.142292]]))
        self.datapoints_2.addDatapoint(
            Datapoint.from_values(
                Value(0.6, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                Value(0.5, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                "EXAMPLE4",
            )
        )
        actual_value = self.datapoints_2.polynomial_fit_with_covar(0)
        LOGGER.debug(actual_value)
        for i in range(len(expected_value_tuple)):
            np.testing.assert_array_almost_equal(
                expected_value_tuple[i], actual_value[i]
            )

    def test_polynomial_fit(self):
        """Test to see if expected results are produced.
        """
        expected_value = np.array([-0.0500, 1.07142857])
        actual_value = self.datapoints_2.polynomial_fit(1)
        np.testing.assert_array_almost_equal(expected_value, actual_value)

    def test_polynomial_fit_covar(self):
        """Test to see if expected results are produced.
        """
        expected_value = np.array([0.000714])
        actual_value = self.datapoints_2.polynomial_fit_covar(1)
        np.testing.assert_array_almost_equal(expected_value, actual_value)

    def test_polynomial_fit_rmse(self):
        """Test to see if expected RMSE for polynomial fit is returned.
        """
        expected_value = 0.015430334996209221
        actual_value = self.datapoints_2.polynomial_fit_rmse(1)
        self.assertAlmostEqual(expected_value, actual_value)

    def test_poly_fit_data(self):
        """Test to see if expected data is returned.
        """
        expected_dict = {
            "coefficients": np.array([-0.0500, 1.07142857]),
            "RMSE": 0.015430334996209221,
            "order": 1,
            "covar": np.array([0.000714]),
        }
        actual_dict = self.datapoints_2.poly_fit_data(1)
        self.assertListEqual(sorted(expected_dict.keys()), sorted(actual_dict.keys()))
        for key in expected_dict.keys():
            if key == "RMSE":
                self.assertAlmostEqual(expected_dict[key], actual_dict[key])
            elif key == "order":
                self.assertEqual(expected_dict[key], actual_dict[key])
            else:
                np.testing.assert_array_almost_equal(
                    expected_dict[key], actual_dict[key]
                )

    def test_create_datapoint_sub_group(self):
        """Test to see if expected sub group is created.
        """
        actual_sub_group = self.datapoints_2.createDatapointsSubGroup(["EXAMPLE1"])
        self.datapoints_1.addDatapoint(
            Datapoint.from_values(
                Value(1.0, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                Value(1.0, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                "EXAMPLE1",
            )
        )
        expected_sub_group = self.datapoints_1
        self.assertEqual(actual_sub_group, expected_sub_group)

    def test_plot_ranges(self):
        """Test to see if expected plot ranges are returned.
        """
        expected_plot_ranges = np.array([0.5, 2.0, 0.5, 2.1])
        actual_plot_ranges = np.array(self.datapoints_2.plotRanges())
        np.testing.assert_array_almost_equal(actual_plot_ranges, expected_plot_ranges)

    def test_plot_parameters(self):
        """Test to see if expected dictionaries are produced.
        """
        expected_dict = {
            "y_data": np.array([1.0, 2.1, 0.5]),
            "y_label": "$Log(Sw)_{Calc}$",
            "x_data": np.array([1.0, 2.0, 0.5]),
            "x_label": "$Log(Sw)_{Exp}$",
            "figure_label": "Log(Sw)_ExpvsLog(Sw)_Calc",
            "plot_axis_range": [0.5, 2.0, 0.5, 2.1],
            "x_axis_range": [0.50, 2.0],
            "y_axis_range": [0.50, 2.1],
        }
        actual_dict = self.datapoints_2.generateScatterPlotParameters()
        self.assertListEqual(sorted(actual_dict.keys()), sorted(expected_dict.keys()))
        for key in actual_dict.keys():
            if key == "y_data" or key == "x_data":
                np.testing.assert_array_almost_equal(
                    actual_dict[key], expected_dict[key]
                )
            elif (
                key == "plot_axis_range"
                or key == "x_axis_range"
                or key == "y_axis_range"
            ):
                np.testing.assert_array_almost_equal(
                    np.array(actual_dict[key]), np.array(expected_dict[key])
                )
            else:
                LOGGER.debug("assert equal string")
                self.assertEqual(actual_dict[key], expected_dict[key])

    def test_plot_parameters_polynomial(self):
        """Test to see if expected dictionary of parameters is produced.
        """
        expected_dict = {
            "y_data": np.array([1.0, 2.1, 0.5]),
            "y_label": "$Log(Sw)_{Calc}$",
            "x_data": np.array([1.0, 2.0, 0.5]),
            "x_label": "$Log(Sw)_{Exp}$",
            "figure_label": "Log(Sw)_ExpvsLog(Sw)_Calc",
            "plot_axis_range": [0.5, 2.0, 0.5, 2.1],
            "x_axis_range": [0.50, 2.0],
            "y_axis_range": [0.50, 2.1],
            "polynomial order": 1,
            "polynomial coefficients": np.array([-0.0500, 1.07142857]),
        }
        actual_dict = self.datapoints_2.generateScatterPlotParameters(
            polynomial_order=1
        )
        self.assertListEqual(sorted(actual_dict.keys()), sorted(expected_dict.keys()))
        for key in actual_dict.keys():
            LOGGER.debug("Key: %s", key)
            if key == "y_data" or key == "x_data" or key == "polynomial coefficients":
                LOGGER.debug("assert equal array x and y data")
                np.testing.assert_array_almost_equal(
                    actual_dict[key], expected_dict[key]
                )
            elif (
                key == "plot_axis_range"
                or key == "x_axis_range"
                or key == "y_axis_range"
            ):
                np.testing.assert_array_almost_equal(
                    np.array(actual_dict[key]), np.array(expected_dict[key])
                )
            else:
                LOGGER.debug("assert equal string")
                self.assertEqual(actual_dict[key], expected_dict[key])

    def test_fromdatapoints(self):
        """Test to see if constructor function returns expected Datapoints object.
        """
        expected_datapoints = self.datapoints_2
        actual_datapoints = Datapoints.fromdatapoints(
            [
                Datapoint.from_values(
                    Value(1.0, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(1.0, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                    "EXAMPLE1",
                ),
                Datapoint.from_values(
                    Value(2.0, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(2.1, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                    "EXAMPLE2",
                ),
                Datapoint.from_values(
                    Value(0.5, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(0.5, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                    "EXAMPLE3",
                ),
            ]
        )
        self.assertEqual(actual_datapoints, expected_datapoints)
