#    resultsanalysis provides generic tools and classes for analysis of calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    resultsanalysis is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script contains test case for the Molecule class

@author: mark
"""

import unittest
import logging
import re
from resultsanalysis.dataclasses.units import Units
from resultsanalysis.dataclasses.value import Value
from resultsanalysis.dataclasses.valuetype import ValueType
from resultsanalysis.dataclasses.molecule import Molecule
from resultsanalysis.dataclasses.datapoint import Datapoint
from rdkit import Chem


logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class MoleculeTestCase(unittest.TestCase):
    """Test case for the Datapoint object.
    """

    def setUp(self):
        """Set up for tests
        """
        self.mol_water = Molecule.from_moldescriptors(
            "XLYOFNOQVPJJNP-UHFFFAOYSA-N", "water", "O"
        )
        self.mol_methanol = Molecule.from_moldescriptors(
            "OKKJLVBELUTLKV-UHFFFAOYSA-N", "Methanol", "CO"
        )
        self.datapoint_1 = Datapoint.from_values(
            Value(1.0, "Log(Sw)", ValueType.experimental, Units.dimensionless),
            Value(1.0, "Log(Sw)", ValueType.calculated, Units.dimensionless),
            "OKKJLVBELUTLKV-UHFFFAOYSA-N",
        )
        self.alcohol_group = Chem.MolFromSmiles("CO")

    def tearDown(self):
        """Tear down for tests.
        """
        del self.mol_water
        del self.mol_methanol
        del self.datapoint_1

    def test___str__(self):
        """Test to see if expected string is returned.
        """
        expected_string = r"""Molecule\(                              name SMILES \s+ MolRep  Number_of_Rings Functional_Groups[\n\r\s]+XLYOFNOQVPJJNP-UHFFFAOYSA-N  water      O  <rdkit\.Chem\.rdchem\.Mol object at \w+>                0                \{\}\)"""
        actual_string = str(self.mol_water)
        self.assertTrue(re.match(expected_string, actual_string))

    def test_add_datapoint(self):
        """Test to see if datapoint is added as expected.
        """
        self.mol_methanol.addDatapoint(self.datapoint_1)
        expected_mol = Molecule.create_molecule_with_datapoints(
            ["OKKJLVBELUTLKV-UHFFFAOYSA-N", "Methanol", "CO"],
            [
                Datapoint.from_values(
                    Value(1.0, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(1.0, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                    "OKKJLVBELUTLKV-UHFFFAOYSA-N",
                )
            ],
        )
        actual_datapoint_dict = self.mol_methanol
        self.assertEqual(actual_datapoint_dict, expected_mol)

    def test_has_datapoint(self):
        """Test to see if molecule has datapoint.
        """
        self.assertFalse(
            self.mol_methanol.hasDatapoint(("Log(Sw)_{Calc}", "Log(Sw)_{Exp}"))
        )
        self.mol_methanol.addDatapoint(self.datapoint_1)
        self.assertTrue(
            self.mol_methanol.hasDatapoint(("Log(Sw)_{Calc}", "Log(Sw)_{Exp}"))
        )

    def test_abs_diff_datapoint(self):
        """Test to see if expected diff is returned.
        """
        expected_diff = 0.0
        self.mol_methanol.addDatapoint(self.datapoint_1)
        actual_diff = self.mol_methanol.absDiffDatapoint(
            ("Log(Sw)_{Calc}", "Log(Sw)_{Exp}")
        )
        self.assertAlmostEqual(actual_diff, expected_diff)

    def test_search_for_sub_structure(self):
        """Test to see if substructure is present.
        """
        self.assertFalse(self.mol_water.searchForSubStructure(self.alcohol_group))
        self.assertTrue(self.mol_methanol.searchForSubStructure(self.alcohol_group))

    def test_search_for_func_group(self):
        """test to see if expected functional group is found and added to dictionary.
        """
        self.mol_methanol.searchForFunctionalGroup("alcohol", self.alcohol_group)
        expected_func_group_dict = {"alcohol": 1}
        actual_func_group_dict = self.mol_methanol.data["Functional_Groups"].loc[
            self.mol_methanol.data.index[0]
        ]
        self.assertDictEqual(actual_func_group_dict, expected_func_group_dict)

    def test_search_for_func_groups(self):
        """Test to see if expected functional groups are found.
        """
        self.mol_methanol.searchForFunctionalGroups({"alcohol": self.alcohol_group})
        expected_func_group_dict = {"alcohol": 1}
        actual_func_group_dict = self.mol_methanol.data["Functional_Groups"].loc[
            self.mol_methanol.data.index[0]
        ]
        self.assertDictEqual(actual_func_group_dict, expected_func_group_dict)

    def test_from_moldescriptors(self):
        """Test to see if expected molecule is produced.
        """
        actual_molecule = Molecule.from_moldescriptors(
            "XLYOFNOQVPJJNP-UHFFFAOYSA-N", "water", "O"
        )
        expected_molecule = self.mol_water
        self.assertEqual(actual_molecule, expected_molecule)

    def test_create_molecule_w_datapoints(self):
        """Test to see if expected molecule has been created.
        """
        actual_molecule = Molecule.create_molecule_with_datapoints(
            ["OKKJLVBELUTLKV-UHFFFAOYSA-N", "Methanol", "CO"],
            [
                Datapoint.from_values(
                    Value(1.0, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(1.0, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                    "OKKJLVBELUTLKV-UHFFFAOYSA-N",
                )
            ],
        )
        self.mol_methanol.addDatapoint(self.datapoint_1)
        expected_molecule = self.mol_methanol
        self.assertEqual(actual_molecule, expected_molecule)
