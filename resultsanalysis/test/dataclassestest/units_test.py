#    resultsanalysis provides generic tools and classes for analysis of calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    resultsanalysis is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Test case for the Units class.

@author: mark
"""

import unittest
import logging
import resultsanalysis.dataclasses.units as Units

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class UnitsTestCase(unittest.TestCase):
    """Test case for the DistanceUnit class.
    """

    def setUp(self):
        """set up for tests.
        """
        self.units_dimensionless = Units.Units(1)
        self.units_au_potential = Units.Units(2)
        self.units_volts = Units.Units(3)
        self.units_au_energy = Units.Units(4)
        self.units_kj_per_mol = Units.Units(5)
        self.units_kcal_per_mol = Units.Units(6)

    def tearDown(self):
        """Tear down for tests.
        """
        del self.units_dimensionless
        del self.units_au_potential
        del self.units_au_energy
        del self.units_volts
        del self.units_kcal_per_mol
        del self.units_kj_per_mol

    def test___repr__(self):
        """Test to see if expected repr string is returned.
        """
        expected_string = "Units.dimensionless"
        actual_string = repr(self.units_dimensionless)
        self.assertEqual(actual_string, expected_string)

    def test_describe(self):
        """Test to see if expected values are returned
        """
        expected_name = "atomic_units_potential"
        expected_value = 2
        actual_name, actual_value = self.units_au_potential.describe()
        self.assertEqual(actual_value, expected_value)
        self.assertEqual(actual_name, expected_name)

    def test_conversion(self):
        """Test to see if expected conversion factors are returned, or correct
        error is raised.
        """
        # test for self conversion.
        expected_con_factor_1 = 1.0
        self.assertEqual(
            self.units_au_energy.conversionFactor(self.units_au_energy),
            expected_con_factor_1,
        )
        self.assertEqual(
            self.units_au_potential.conversionFactor(
                Units.Units.atomic_units_potential
            ),
            expected_con_factor_1,
        )
        self.assertEqual(
            self.units_volts.conversionFactor(Units.Units.volts), expected_con_factor_1
        )
        self.assertEqual(
            self.units_kcal_per_mol.conversionFactor(Units.Units.kcal_per_mol),
            expected_con_factor_1,
        )
        self.assertEqual(
            self.units_kj_per_mol.conversionFactor(Units.Units.kj_per_mol),
            expected_con_factor_1,
        )
        # Test for potential energies
        expected_con_factor_2 = 27.21138505
        self.assertEqual(
            self.units_au_potential.conversionFactor(self.units_volts),
            expected_con_factor_2,
        )
        self.assertEqual(
            self.units_volts.conversionFactor(self.units_au_potential),
            1.0 / expected_con_factor_2,
        )
        # test for energies
        expected_con_factor_3 = 2625.49962
        self.assertEqual(
            self.units_au_energy.conversionFactor(self.units_kj_per_mol),
            expected_con_factor_3,
        )
        self.assertEqual(
            self.units_kj_per_mol.conversionFactor(self.units_au_energy),
            1.0 / expected_con_factor_3,
        )
        expected_con_factor_4 = 627.509
        self.assertEqual(
            self.units_au_energy.conversionFactor(self.units_kcal_per_mol),
            expected_con_factor_4,
        )
        self.assertEqual(
            self.units_kcal_per_mol.conversionFactor(self.units_au_energy),
            1.0 / expected_con_factor_4,
        )
        expected_con_factor_5 = 4.184
        self.assertEqual(
            self.units_kcal_per_mol.conversionFactor(self.units_kj_per_mol),
            expected_con_factor_5,
        )
        self.assertEqual(
            self.units_kj_per_mol.conversionFactor(self.units_kcal_per_mol),
            1.0 / expected_con_factor_5,
        )
        with self.assertRaises(TypeError) as err_1:
            self.units_dimensionless.conversionFactor(self.units_au_energy)
        expected_args_1 = "Can't convert to or from dimensionless unit."
        actual_args_1 = err_1.exception.args[0]
        self.assertEqual(actual_args_1, expected_args_1)
        with self.assertRaises(TypeError) as err_2:
            self.units_au_energy.conversionFactor(self.units_au_potential)
        expected_args_2 = "Can't convert between these units."
        actual_args_2 = err_2.exception.args[0]
        self.assertEqual(actual_args_2, expected_args_2)

    def test_write_label(self):
        """Test to see if the correct unit label for figures is written.
        """
        self.assertEqual(self.units_dimensionless.unit_label(), "")
        self.assertEqual(self.units_au_potential.unit_label(), "/A.U.")
        self.assertEqual(self.units_volts.unit_label(), "/V")
        self.assertEqual(self.units_au_energy.unit_label(), "/A.U.")
        self.assertEqual(self.units_kcal_per_mol.unit_label(), "/kcalmol^{-1}")
        self.assertEqual(self.units_kj_per_mol.unit_label(), "/kJmol^{-1}")

    def test_unit_from_string(self):
        """Test to see if expected unit is returned from a string.
        """
        actual_unit_1 = Units.createUnitsFromString("dimensionless")
        self.assertEqual(actual_unit_1, self.units_dimensionless)
        actual_unit_2 = Units.createUnitsFromString("atomic_units_energy")
        self.assertEqual(actual_unit_2, self.units_au_energy)
        actual_unit_3 = Units.createUnitsFromString("atomic_units_potential")
        self.assertEqual(actual_unit_3, self.units_au_potential)
        actual_unit_4 = Units.createUnitsFromString("volts")
        self.assertEqual(actual_unit_4, self.units_volts)
        actual_unit_5 = Units.createUnitsFromString("kj_per_mol")
        self.assertEqual(actual_unit_5, self.units_kj_per_mol)
        actual_unit_6 = Units.createUnitsFromString("kcal_per_mol")
        self.assertEqual(actual_unit_6, self.units_kcal_per_mol)
