#    resultsanalysis provides generic tools and classes for analysis of calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    resultsanalysis is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script contains tests for the Value class.

@author: mdd31
"""

import unittest
import logging
from resultsanalysis.dataclasses.units import Units
from resultsanalysis.dataclasses.value import Value
from resultsanalysis.dataclasses.valuetype import ValueType

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class ValueTestCase(unittest.TestCase):
    """Test case for the Value class.
    """

    def setUp(self):
        """set up for tests.
        """
        self.value_1 = Value(
            1.0, "Log(Sw)", ValueType.experimental, Units.dimensionless
        )
        self.value_2 = Value(
            1.0, "E_{max}", ValueType.experimental, Units.atomic_units_potential
        )
        self.value_3 = Value(27.211385, "E_{max}", ValueType.experimental, Units.volts)

    def tearDown(self):
        """Tear down for tests.
        """
        del self.value_1
        del self.value_2
        del self.value_3

    def test___repr__(self):
        """Test to see if expected repr is returned.
        """
        expected_repr = (
            "Value(1.0, 'Log(Sw)', ValueType.experimental, Units.dimensionless)"
        )
        actual_repr = repr(self.value_1)
        self.assertEqual(actual_repr, expected_repr)

    def test_abs_diff(self):
        """Test to see if expected abs_diff is raised.
        """
        expected_diff = 0.0
        actual_diff = self.value_2.abs_diff(self.value_2)
        self.assertEqual(actual_diff, expected_diff)
        with self.assertRaises(TypeError) as err_1:
            self.value_2.abs_diff(self.value_1)
        expected_args_1 = "Value Types do not match."
        actual_args_1 = err_1.exception.args[0]
        self.assertEqual(actual_args_1, expected_args_1)
        with self.assertRaises(TypeError) as err_2:
            self.value_2.abs_diff(self.value_3)
        expected_args_2 = "Correct value type, but units do not match."
        actual_args_2 = err_2.exception.args[0]
        self.assertEqual(actual_args_2, expected_args_2)

    def test_convert_value(self):
        """Test to see if conversion is carried out as expected.
        """
        expected_value = self.value_3
        actual_value = self.value_2.convert_value(Units.volts)
        self.assertEqual(expected_value, actual_value)
        with self.assertRaises(TypeError) as err:
            self.value_1.convert_value(Units.atomic_units_energy)
        expected_args = "Can't convert to or from dimensionless unit."
        actual_args = err.exception.args[0]
        self.assertEqual(actual_args, expected_args)
