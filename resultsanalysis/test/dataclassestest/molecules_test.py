#    resultsanalysis provides generic tools and classes for analysis of calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    resultsanalysis is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script contains test for the molecules class.

@author: mark
"""

import unittest
import logging
import numpy as np
import pathlib
from resultsanalysis.dataclasses.units import Units
from resultsanalysis.dataclasses.value import Value
from resultsanalysis.dataclasses.valuetype import ValueType
from resultsanalysis.dataclasses.molecule import Molecule
from resultsanalysis.dataclasses.molecules import Molecules
from resultsanalysis.dataclasses.datapoint import Datapoint
from resultsanalysis.dataclasses.datapoints import Datapoints
import resultsanalysis.fileparsing.smarts_string_sorter as SmartsStringSorter
import resultsanalysis.fileparsing.textfilereader as TextFileReader

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class MoleculesTestCase(unittest.TestCase):
    """Test case for the Datapoint object.
    """

    def setUp(self):
        """Set up for tests
        """
        parent_directory = pathlib.Path(__file__).parents[1]
        self.datafilename = (
            (parent_directory / "resources/test_results.csv").absolute().as_posix()
        )
        self.empty_molecule = Molecules()
        self.molecules_test = TextFileReader.parseCSVDataFile(self.datafilename, "\t")
        smartsfilename = (
            (parent_directory / "resources/SmartsStrings2.txt").absolute().as_posix()
        )
        smarts_list = SmartsStringSorter.readSmartsFromFile(smartsfilename)
        self.fragment_list = SmartsStringSorter.SmartsFragmentsList()
        self.fragment_list.addSmartsStringList(smarts_list)
        self.molecules_test.addFragmentList(self.fragment_list)

    def tearDown(self):
        """Tear down for tests.
        """
        del self.molecules_test
        del self.empty_molecule
        del self.fragment_list

    def test___str__(self):
        """Test to see if string is returned as expected.
        """
        expected_string = "Molecules()"
        actual_string = str(self.empty_molecule)
        self.assertEqual(actual_string, expected_string)

    def test_add_molecule(self):
        """Test to see if molecule is added as expected.
        """
        mol_to_add = Molecule.create_molecule_with_datapoints(
            ["AFABGHUZZDYHJO-UHFFFAOYSA-N", "2-methylpentane", "CCCC(C)C"],
            [
                Datapoint.from_values(
                    Value(-3.7, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(
                        -3.8114, "Log(Sw)", ValueType.calculated, Units.dimensionless
                    ),
                    "AFABGHUZZDYHJO-UHFFFAOYSA-N",
                )
            ],
        )
        self.empty_molecule.addMolecule(mol_to_add)
        mol = Molecule.create_molecule_with_datapoints(
            ["AFABGHUZZDYHJO-UHFFFAOYSA-N", "2-methylpentane", "CCCC(C)C"],
            [
                Datapoint.from_values(
                    Value(-3.7, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(
                        -3.8114, "Log(Sw)", ValueType.calculated, Units.dimensionless
                    ),
                    "AFABGHUZZDYHJO-UHFFFAOYSA-N",
                )
            ],
        )
        expected_mol = Molecules(data=mol.data)
        self.assertEqual(self.empty_molecule, expected_mol)

    def test_add_mol_list(self):
        """Test to see if expected molecules are added.
        """
        mol_list_to_add = [
            Molecule.create_molecule_with_datapoints(
                ["AFABGHUZZDYHJO-UHFFFAOYSA-N", "2-methylpentane", "CCCC(C)C"],
                [
                    Datapoint.from_values(
                        Value(
                            -3.7, "Log(Sw)", ValueType.experimental, Units.dimensionless
                        ),
                        Value(
                            -3.8114,
                            "Log(Sw)",
                            ValueType.calculated,
                            Units.dimensionless,
                        ),
                        "AFABGHUZZDYHJO-UHFFFAOYSA-N",
                    )
                ],
            )
        ]
        self.empty_molecule.addMoleculeList(mol_list_to_add)
        mol = Molecule.create_molecule_with_datapoints(
            ["AFABGHUZZDYHJO-UHFFFAOYSA-N", "2-methylpentane", "CCCC(C)C"],
            [
                Datapoint.from_values(
                    Value(-3.7, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(
                        -3.8114, "Log(Sw)", ValueType.calculated, Units.dimensionless
                    ),
                    "AFABGHUZZDYHJO-UHFFFAOYSA-N",
                )
            ],
        )
        expected_mol = Molecules(data=mol.data)
        self.assertEqual(self.empty_molecule, expected_mol)

    def test_gen_dat_point_list(self):
        """Test to see fi expected datapoint list is created.
        """
        mol_list_to_add = [
            Molecule.create_molecule_with_datapoints(
                ["AFABGHUZZDYHJO-UHFFFAOYSA-N", "2-methylpentane", "CCCC(C)C"],
                [
                    Datapoint.from_values(
                        Value(
                            -3.7, "Log(Sw)", ValueType.experimental, Units.dimensionless
                        ),
                        Value(
                            -3.8114,
                            "Log(Sw)",
                            ValueType.calculated,
                            Units.dimensionless,
                        ),
                        "AFABGHUZZDYHJO-UHFFFAOYSA-N",
                    )
                ],
            )
        ]
        self.empty_molecule.addMoleculeList(mol_list_to_add)
        actual_datapoints = self.empty_molecule.generate_datapoints(
            ("Log(Sw)_{Exp}", "Log(Sw)_{Calc}")
        )
        expected_datapoints = Datapoints()
        expected_datapoints.addDatapoint(
            Datapoint.from_values(
                Value(-3.7, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                Value(-3.8114, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                "AFABGHUZZDYHJO-UHFFFAOYSA-N",
            )
        )
        self.assertEqual(actual_datapoints, expected_datapoints)

    def test_add_fragment_list(self):
        """Test to see if expected fragment lsit is added.
        """
        expected_smarts_fragment = self.fragment_list
        actual_smarts_fragment = self.molecules_test.fragment_list
        self.assertEqual(actual_smarts_fragment, expected_smarts_fragment)

    def test_find_functional_groups(self):
        """Test to see if expected functional groups are found.
        """
        expected_functional_groups = {
            "Primary_alcohol": [
                "AMQJEAYHLZJPGS-UHFFFAOYSA-N",
                "BDERNNFJNOPAEC-UHFFFAOYSA-N",
                "ZXEKIIBDNHEJCQ-UHFFFAOYSA-N",
            ],
            "Secondary_alcohol": [
                "AQIXEPGDORPWBJ-UHFFFAOYSA-N",
                "BTANRVKWQNVYAZ-SCSAIBSYSA-N",
            ],
        }
        actual_functional_groups = self.molecules_test.findFunctionalGroups()
        self.assertEqual(actual_functional_groups, expected_functional_groups)

    def test_find_ring_sizes(self):
        """Test to see if expected ring sizes are found for test molecules.
        """
        expected_ring_sizes = {
            0: [
                "AFABGHUZZDYHJO-UHFFFAOYSA-N",
                "AMQJEAYHLZJPGS-UHFFFAOYSA-N",
                "AQIXEPGDORPWBJ-UHFFFAOYSA-N",
                "ATUOYWHBWRKTHZ-UHFFFAOYSA-N",
                "BDERNNFJNOPAEC-UHFFFAOYSA-N",
                "BTANRVKWQNVYAZ-SCSAIBSYSA-N",
                "BZHMBWZPUJHVEE-UHFFFAOYSA-N",
                "OKKJLVBELUTLKV-UHFFFAOYSA-N",
                "ZXEKIIBDNHEJCQ-UHFFFAOYSA-N",
            ]
        }
        actual_ring_sizes = self.molecules_test.findRingSizes()
        self.assertDictEqual(actual_ring_sizes, expected_ring_sizes)

    def test_regression_analysis(self):
        """Test to see if expected regression analysis is returned for all data.
        """
        expected_reg_analysis = (
            ("Log(Sw)_{Exp}", "Log(Sw)_{Calc}"),
            {"y": Units.dimensionless, "x": Units.dimensionless},
            (
                0.91168297641380702,
                -0.18689535313025607,
                0.96770747909809207,
                0.37557321431514801,
            ),
        )
        LOGGER.debug("value types:")
        LOGGER.debug(self.molecules_test.data.index)
        actual_reg_analysis = self.molecules_test.regressionAnalysis(
            ("Log(Sw)_{Exp}", "Log(Sw)_{Calc}")
        )

        LOGGER.debug("actual regression analysis")
        LOGGER.debug(actual_reg_analysis)
        self.assertEqual(actual_reg_analysis[:2], expected_reg_analysis[:2])
        np.testing.assert_array_almost_equal(
            np.array(actual_reg_analysis[2]), np.array(expected_reg_analysis[2])
        )

    def test_data_by_func_group(self):
        """Test to see if expected dictionary of datapoints is returned per
        functional group.
        """
        self.maxDiff = None
        expected_dict = {
            "Primary_alcohol": Datapoints.fromdatapoints(
                [
                    Datapoint.from_values(
                        Value(
                            -0.6, "Log(Sw)", ValueType.experimental, Units.dimensionless
                        ),
                        Value(
                            -0.4111337476,
                            "Log(Sw)",
                            ValueType.calculated,
                            Units.dimensionless,
                        ),
                        "AMQJEAYHLZJPGS-UHFFFAOYSA-N",
                    ),
                    Datapoint.from_values(
                        Value(
                            0.6, "Log(Sw)", ValueType.experimental, Units.dimensionless
                        ),
                        Value(
                            0.6388444768,
                            "Log(Sw)",
                            ValueType.calculated,
                            Units.dimensionless,
                        ),
                        "BDERNNFJNOPAEC-UHFFFAOYSA-N",
                    ),
                    Datapoint.from_values(
                        Value(
                            0.2, "Log(Sw)", ValueType.experimental, Units.dimensionless
                        ),
                        Value(
                            -0.0780129341,
                            "Log(Sw)",
                            ValueType.calculated,
                            Units.dimensionless,
                        ),
                        "ZXEKIIBDNHEJCQ-UHFFFAOYSA-N",
                    ),
                ]
            ),
            "Secondary_alcohol": Datapoints.fromdatapoints(
                [
                    Datapoint.from_values(
                        Value(
                            -0.2, "Log(Sw)", ValueType.experimental, Units.dimensionless
                        ),
                        Value(
                            -1.00398538,
                            "Log(Sw)",
                            ValueType.calculated,
                            Units.dimensionless,
                        ),
                        "AQIXEPGDORPWBJ-UHFFFAOYSA-N",
                    ),
                    Datapoint.from_values(
                        Value(
                            0.5, "Log(Sw)", ValueType.experimental, Units.dimensionless
                        ),
                        Value(
                            -0.0458138847,
                            "Log(Sw)",
                            ValueType.calculated,
                            Units.dimensionless,
                        ),
                        "BTANRVKWQNVYAZ-SCSAIBSYSA-N",
                    ),
                ]
            ),
        }
        actual_dict = self.molecules_test.dataByFunctionalGroup(
            ("Log(Sw)_{Exp}", "Log(Sw)_{Calc}")
        )
        self.assertDictEqual(actual_dict, expected_dict)

    def test_data_by_rings(self):
        """Test to see if expected dictionary of datapoints is returned per number
        of rings.
        """
        self.maxDiff = None
        expected_dict = {
            0: Datapoints.fromdatapoints(
                [
                    Datapoint.from_values(
                        Value(
                            -3.7, "Log(Sw)", ValueType.experimental, Units.dimensionless
                        ),
                        Value(
                            -3.8114199738,
                            "Log(Sw)",
                            ValueType.calculated,
                            Units.dimensionless,
                        ),
                        "AFABGHUZZDYHJO-UHFFFAOYSA-N",
                    ),
                    Datapoint.from_values(
                        Value(
                            -0.6, "Log(Sw)", ValueType.experimental, Units.dimensionless
                        ),
                        Value(
                            -0.4111337476,
                            "Log(Sw)",
                            ValueType.calculated,
                            Units.dimensionless,
                        ),
                        "AMQJEAYHLZJPGS-UHFFFAOYSA-N",
                    ),
                    Datapoint.from_values(
                        Value(
                            -0.2, "Log(Sw)", ValueType.experimental, Units.dimensionless
                        ),
                        Value(
                            -1.00398538,
                            "Log(Sw)",
                            ValueType.calculated,
                            Units.dimensionless,
                        ),
                        "AQIXEPGDORPWBJ-UHFFFAOYSA-N",
                    ),
                    Datapoint.from_values(
                        Value(
                            -1.9, "Log(Sw)", ValueType.experimental, Units.dimensionless
                        ),
                        Value(
                            -1.7131181741,
                            "Log(Sw)",
                            ValueType.calculated,
                            Units.dimensionless,
                        ),
                        "ATUOYWHBWRKTHZ-UHFFFAOYSA-N",
                    ),
                    Datapoint.from_values(
                        Value(
                            0.6, "Log(Sw)", ValueType.experimental, Units.dimensionless
                        ),
                        Value(
                            0.6388444768,
                            "Log(Sw)",
                            ValueType.calculated,
                            Units.dimensionless,
                        ),
                        "BDERNNFJNOPAEC-UHFFFAOYSA-N",
                    ),
                    Datapoint.from_values(
                        Value(
                            0.5, "Log(Sw)", ValueType.experimental, Units.dimensionless
                        ),
                        Value(
                            -0.0458138847,
                            "Log(Sw)",
                            ValueType.calculated,
                            Units.dimensionless,
                        ),
                        "BTANRVKWQNVYAZ-SCSAIBSYSA-N",
                    ),
                    Datapoint.from_values(
                        Value(
                            -4.3, "Log(Sw)", ValueType.experimental, Units.dimensionless
                        ),
                        Value(
                            -3.9014090428,
                            "Log(Sw)",
                            ValueType.calculated,
                            Units.dimensionless,
                        ),
                        "BZHMBWZPUJHVEE-UHFFFAOYSA-N",
                    ),
                    Datapoint.from_values(
                        Value(
                            1.6, "Log(Sw)", ValueType.experimental, Units.dimensionless
                        ),
                        Value(
                            1.5328632661,
                            "Log(Sw)",
                            ValueType.calculated,
                            Units.dimensionless,
                        ),
                        "OKKJLVBELUTLKV-UHFFFAOYSA-N",
                    ),
                    Datapoint.from_values(
                        Value(
                            0.2, "Log(Sw)", ValueType.experimental, Units.dimensionless
                        ),
                        Value(
                            -0.0780129341,
                            "Log(Sw)",
                            ValueType.calculated,
                            Units.dimensionless,
                        ),
                        "ZXEKIIBDNHEJCQ-UHFFFAOYSA-N",
                    ),
                ]
            )
        }
        self.molecules_test.findRingSizes()
        actual_dict = self.molecules_test.dataByNumberOfRings(
            ("Log(Sw)_{Exp}", "Log(Sw)_{Calc}")
        )
        self.assertDictEqual(actual_dict, expected_dict)

    def test_reg_analysis_by_func_group(self):
        """Test to see if expected dictionary of regression analysis by func group
        is returned.
        """
        expected_reg_analysis = {
            "Primary_alcohol": (
                0.80947030555357136,
                -0.0040654220035714131,
                0.84972814463318036,
                0.19533778112146935,
            ),
            "Secondary_alcohol": (
                1.368816421857143,
                -0.73022209562857143,
                1.0,
                0.68713364347303263,
            ),
        }
        actual_reg_analysis = self.molecules_test.regressionAnalysisPerFunctionalGroup(
            ("Log(Sw)_{Exp}", "Log(Sw)_{Calc}")
        )
        self.assertEqual(actual_reg_analysis.keys(), expected_reg_analysis.keys())
        for func_group, reg_values in actual_reg_analysis.items():
            np.testing.assert_array_almost_equal(
                np.array(reg_values), np.array(expected_reg_analysis[func_group])
            )

    def test_reg_analysis_by_rings(self):
        """Test to see if expected dictionary of regression analysis by number
        of rings is returned.
        """
        expected_reg_analysis = {
            0: (
                0.91168297641380702,
                -0.18689535313025607,
                0.96770747909809207,
                0.37557321431514801,
            )
        }
        self.molecules_test.findRingSizes()
        LOGGER.debug("molecules by ring size:")
        LOGGER.debug(self.molecules_test.findRingSizes())
        actual_reg_analysis = self.molecules_test.regressionAnalysisPerNumberOfRings(
            ("Log(Sw)_{Exp}", "Log(Sw)_{Calc}")
        )
        self.assertEqual(actual_reg_analysis.keys(), expected_reg_analysis.keys())
        for ring_size, reg_values in actual_reg_analysis.items():
            np.testing.assert_array_almost_equal(
                np.array(reg_values), np.array(expected_reg_analysis[ring_size])
            )

        # self.assertDictEqual(actual_reg_analysis, expected_reg_analysis)

    def test_gen_scatter_plot_params_by_rings(self):
        """Test to see if expected dictionary of plot parameters is returned
        for the data by rings.
        """
        self.maxDiff = None
        expected_dict = {
            "y_data": np.array(
                [
                    -3.81141997,
                    -0.41113375,
                    -1.00398538,
                    -1.71311817,
                    0.63884448,
                    -0.04581388,
                    -3.90140904,
                    1.53286327,
                    -0.07801293,
                ]
            ),
            "y_label": "$Log(Sw)_{Calc}$",
            "data_by_num_rings": {
                0: {
                    "y_data": np.array(
                        [
                            -3.81141997,
                            -0.41113375,
                            -1.00398538,
                            -1.71311817,
                            0.63884448,
                            -0.04581388,
                            -3.90140904,
                            1.53286327,
                            -0.07801293,
                        ]
                    ),
                    "y_label": "$Log(Sw)_{Calc}$",
                    "x_data": np.array(
                        [-3.7, -0.6, -0.2, -1.9, 0.6, 0.5, -4.3, 1.6, 0.2]
                    ),
                    "x_label": "$Log(Sw)_{Exp}$",
                    "figure_label": "Log(Sw)_ExpvsLog(Sw)_Calc",
                    "plot_axis_range": [-4.3, 1.6, -3.901409, 1.532863],
                    "x_axis_range": [-4.3, 1.6],
                    "y_axis_range": [-3.901409, 1.532863],
                }
            },
            "x_data": np.array([-3.7, -0.6, -0.2, -1.9, 0.6, 0.5, -4.3, 1.6, 0.2]),
            "x_label": "$Log(Sw)_{Exp}$",
            "figure_label": "Log(Sw)_ExpvsLog(Sw)_CalcByNumRings",
            "plot_axis_range": [-4.3, 1.6, -3.901409, 1.532863],
            "x_axis_range": [-4.3, 1.6],
            "y_axis_range": [-3.901409, 1.532863],
        }
        self.molecules_test.findRingSizes()
        actual_dict = self.molecules_test.generateScatterPlotParametersPerNumberOfRings(
            ("Log(Sw)_{Exp}", "Log(Sw)_{Calc}")
        )
        for key, act_value in actual_dict.items():
            LOGGER.debug("Key")
            LOGGER.debug(key)
            if (key == "y_data") or (key == "x_data"):
                np.testing.assert_array_almost_equal(act_value, expected_dict[key])
            elif key == ("data_by_num_rings"):
                for ring_num, ring_data in act_value.items():
                    for ring_key, ring_value in ring_data.items():
                        LOGGER.debug(ring_key)
                        exp_value = expected_dict[key][ring_num]
                        if (ring_key == "y_data") or (ring_key == "x_data"):
                            np.testing.assert_array_almost_equal(
                                ring_value, exp_value[ring_key]
                            )
                        elif (
                            (ring_key == "plot_axis_range")
                            or (ring_key == "x_axis_range")
                            or (ring_key == "y_axis_range")
                        ):
                            np.testing.assert_array_almost_equal(
                                np.array(ring_value), np.array(exp_value[ring_key])
                            )
                        else:
                            self.assertEqual(ring_value, exp_value[ring_key])
            elif (
                (key == "plot_axis_range")
                or (key == "x_axis_range")
                or (key == "y_axis_range")
            ):
                np.testing.assert_array_almost_equal(
                    np.array(act_value), np.array(expected_dict[key])
                )
            else:
                self.assertEqual(act_value, expected_dict[key])

    def test_gen_scatter_plot_params_by_func_group(self):
        """Test to see if expected dictionary of plot parameters is returned
        for the data by functional group.
        """
        self.maxDiff = None
        expected_dict = {
            "y_data": np.array(
                [
                    -3.81141997,
                    -0.41113375,
                    -1.00398538,
                    -1.71311817,
                    0.63884448,
                    -0.04581388,
                    -3.90140904,
                    1.53286327,
                    -0.07801293,
                ]
            ),
            "y_label": "$Log(Sw)_{Calc}$",
            "data_by_func_group": {
                "Primary_alcohol": {
                    "y_data": np.array([-0.41113375, 0.63884448, -0.07801293]),
                    "y_label": "$Log(Sw)_{Calc}$",
                    "x_data": np.array([-0.6, 0.6, 0.2]),
                    "x_label": "$Log(Sw)_{Exp}$",
                    "figure_label": "Log(Sw)_ExpvsLog(Sw)_Calc",
                    "plot_axis_range": [-0.6, 0.6, -0.411134, 0.638844],
                    "x_axis_range": [-0.6, 0.6],
                    "y_axis_range": [-0.411134, 0.638844],
                },
                "Secondary_alcohol": {
                    "y_data": np.array([-1.00398538, -0.04581388]),
                    "y_label": "$Log(Sw)_{Calc}$",
                    "x_data": np.array([-0.2, 0.5]),
                    "x_label": "$Log(Sw)_{Exp}$",
                    "figure_label": "Log(Sw)_ExpvsLog(Sw)_Calc",
                    "plot_axis_range": [-0.2, 0.5, -1.003985, -0.045814],
                    "x_axis_range": [-0.2, 0.5],
                    "y_axis_range": [-1.003985, -0.045814],
                },
            },
            "x_data": np.array([-3.7, -0.6, -0.2, -1.9, 0.6, 0.5, -4.3, 1.6, 0.2]),
            "x_label": "$Log(Sw)_{Exp}$",
            "figure_label": "Log(Sw)_ExpvsLog(Sw)_CalcByFuncGroup",
            "plot_axis_range": [-4.3, 1.6, -3.901409, 1.532863],
            "x_axis_range": [-4.3, 1.6],
            "y_axis_range": [-3.901409, 1.532863],
        }
        self.molecules_test.findFunctionalGroups()
        actual_dict = self.molecules_test.generateScatterPlotParametersPerFunctionalGroup(
            ("Log(Sw)_{Exp}", "Log(Sw)_{Calc}")
        )
        for key, act_value in actual_dict.items():
            LOGGER.debug("Key")
            LOGGER.debug(key)
            if (key == "y_data") or (key == "x_data"):
                np.testing.assert_array_almost_equal(act_value, expected_dict[key])
            elif key == "data_by_func_group":
                for func_group, func_data in act_value.items():
                    exp_value = expected_dict[key][func_group]
                    for func_key, func_value in func_data.items():
                        if (func_key == "y_data") or (func_key == "x_data"):
                            np.testing.assert_array_almost_equal(
                                func_value, exp_value[func_key]
                            )
                        elif (
                            (func_key == "plot_axis_range")
                            or (func_key == "x_axis_range")
                            or (func_key == "y_axis_range")
                        ):
                            np.testing.assert_array_almost_equal(
                                np.array(func_value), np.array(exp_value[func_key])
                            )
                        else:
                            self.assertEqual(func_value, exp_value[func_key])
            elif (
                (key == "plot_axis_range")
                or (key == "x_axis_range")
                or (key == "y_axis_range")
            ):
                np.testing.assert_array_almost_equal(
                    np.array(act_value), np.array(expected_dict[key])
                )
            else:
                self.assertEqual(act_value, expected_dict[key])

    def test_gen_bar_plot_params_by_rings(self):
        """Test to see if expected dictionary of plot parameters is returned
        for the data by rings.
        """
        expected_dict = {
            "figure_label": "Log(Sw)_ExpvsLog(Sw)_CalcrmseByRings",
            "rmse_values": {0: 0.37557321431514801},
            "unit": {"x": Units.dimensionless, "y": Units.dimensionless},
        }
        self.molecules_test.findRingSizes()
        actual_dict = self.molecules_test.generateBarPlotParametersPerNumberOfRings(
            ("Log(Sw)_{Exp}", "Log(Sw)_{Calc}")
        )
        self.assertDictEqual(actual_dict, expected_dict)

    def test_gen_bar_plot_params_by_func_group(self):
        """Test to see if expected dictionary of plot parameters is returned
        for the data by functional group.
        """
        expected_dict = {
            "figure_label": "Log(Sw)_ExpvsLog(Sw)_CalcrmseByFuncGroup",
            "rmse_values": {
                "Primary_alcohol": 0.19533778112146935,
                "Secondary_alcohol": 0.68713364347303263,
            },
            "unit": {"x": Units.dimensionless, "y": Units.dimensionless},
        }
        self.molecules_test.findFunctionalGroups()
        actual_dict = self.molecules_test.generateBarPlotParametersPerFunctionalGroup(
            ("Log(Sw)_{Exp}", "Log(Sw)_{Calc}")
        )
        self.assertDictEqual(actual_dict, expected_dict)

    def test_gen_reg_data_to_write(self):
        """Test to see if expected dictionary of regression data for writing
        is returned.
        """
        expected_data = {
            "Regress_values": (
                0.91168297641380702,
                -0.18689535313025607,
                0.96770747909809207,
                0.37557321431514801,
            ),
            "filename": "Log(Sw)_ExpvsLog(Sw)_CalcRegress.csv",
            "unit": {"x": Units.dimensionless, "y": Units.dimensionless},
            "value_type": ("Log(Sw)_{Exp}", "Log(Sw)_{Calc}"),
        }
        actual_data = self.molecules_test.generateRegressionDataToWrite(
            ("Log(Sw)_{Exp}", "Log(Sw)_{Calc}")
        )
        self.assertEqual(actual_data.keys(), expected_data.keys())
        for key, value in actual_data.items():
            if key == "Regress_values":
                exp_values = expected_data[key]
                np.testing.assert_array_almost_equal(
                    np.array(value), np.array(exp_values)
                )
            else:
                self.assertEqual(value, expected_data[key])

    def test_gen_reg_data_to_write_func_group(self):
        """Test to see if expected dictionary of regression data by functional
        group for writing is returned.
        """
        expected_data = {
            "Regress_values": {
                "Primary_alcohol": (
                    0.80947030555357136,
                    -0.0040654220035714131,
                    0.84972814463318036,
                    0.19533778112146935,
                ),
                "Secondary_alcohol": (
                    1.368816421857143,
                    -0.73022209562857143,
                    1.0,
                    0.68713364347303263,
                ),
            },
            "filename": "Log(Sw)_ExpvsLog(Sw)_CalcRegressByFunc.csv",
            "unit": {"x": Units.dimensionless, "y": Units.dimensionless},
            "value_type": ("Log(Sw)_{Exp}", "Log(Sw)_{Calc}"),
        }
        self.molecules_test.findFunctionalGroups()
        actual_data = self.molecules_test.generateRegressionDataToWritePerFunctionalGroup(
            ("Log(Sw)_{Exp}", "Log(Sw)_{Calc}")
        )
        self.assertEqual(actual_data.keys(), expected_data.keys())
        for key, value in actual_data.items():
            if key == "Regress_values":
                exp_values = expected_data[key]
                for func_group, reg_values in value.items():
                    np.testing.assert_array_almost_equal(
                        np.array(reg_values), np.array(exp_values[func_group])
                    )
            else:
                self.assertEqual(value, expected_data[key])

    def test_gen_reg_data_to_write_rings(self):
        """Test to see if expected dictionary of regression data by number
        of rings for writing is returned.
        """
        expected_data = {
            "Regress_values": {
                0: (
                    0.91168297641380702,
                    -0.18689535313025607,
                    0.96770747909809207,
                    0.37557321431514801,
                )
            },
            "filename": "Log(Sw)_ExpvsLog(Sw)_CalcRegressByRings.csv",
            "unit": {"x": Units.dimensionless, "y": Units.dimensionless},
            "value_type": ("Log(Sw)_{Exp}", "Log(Sw)_{Calc}"),
        }
        self.molecules_test.findRingSizes()
        actual_data = self.molecules_test.generateRegressionDataToWritePerNumberOfRings(
            ("Log(Sw)_{Exp}", "Log(Sw)_{Calc}")
        )
        self.assertEqual(actual_data.keys(), expected_data.keys())
        for key, value in actual_data.items():
            if key == "Regress_values":
                exp_values = expected_data[key]
                for num_rings, reg_values in value.items():
                    np.testing.assert_array_almost_equal(
                        np.array(reg_values), np.array(exp_values[num_rings])
                    )
            else:
                self.assertEqual(value, expected_data[key])
