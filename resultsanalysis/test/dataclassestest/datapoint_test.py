#    resultsanalysis provides generic tools and classes for analysis of calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    resultsanalysis is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  6 14:06:52 2016

@author: mark
"""

import unittest
import logging
from resultsanalysis.dataclasses.units import Units
from resultsanalysis.dataclasses.value import Value
from resultsanalysis.dataclasses.valuetype import ValueType
from resultsanalysis.dataclasses.datapoint import Datapoint

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class DatapointTestCase(unittest.TestCase):
    """Test case for the Datapoint object.
    """

    def setUp(self):
        """Set up for tests
        """
        self.maxDiff = None
        self.datapoint_1 = Datapoint.from_values(
            Value(1.0, "Log(Sw)", ValueType.experimental, Units.dimensionless),
            Value(1.0, "Log(Sw)", ValueType.calculated, Units.dimensionless),
            "EXAMPLE",
        )
        self.datapoint_2 = Datapoint.from_values(
            Value(1.0, "Log(Sw)", ValueType.experimental, Units.kcal_per_mol),
            Value(1.0, "Log(Sw)", ValueType.calculated, Units.kcal_per_mol),
            "EXAMPLE",
        )
        self.datapoint_3 = Datapoint.from_values(
            Value(4.184, "Log(Sw)", ValueType.experimental, Units.kj_per_mol),
            Value(4.184, "Log(Sw)", ValueType.calculated, Units.kj_per_mol),
            "EXAMPLE",
        )

    def tearDown(self):
        """Tear down for tests.
        """
        del self.datapoint_1
        del self.datapoint_2
        del self.datapoint_3

    def test___repr__(self):
        """Test to see if expected representation of Datapoint is returned.
        """
        expected_repr = """Datapoint(         Log(Sw)_{Exp}    Log(Sw)_{Exp}Unit  Log(Sw)_{Calc}   Log(Sw)_{Calc}Unit
EXAMPLE            1.0  Units.dimensionless             1.0  Units.dimensionless)"""
        actual_repr = repr(self.datapoint_1)
        self.assertEqual(expected_repr, actual_repr)

    def test___str__(self):
        """Test to see if expected string is returned.
        """
        expected_str = """         Log(Sw)_{Exp}    Log(Sw)_{Exp}Unit  Log(Sw)_{Calc}   Log(Sw)_{Calc}Unit
EXAMPLE            1.0  Units.dimensionless             1.0  Units.dimensionless"""
        actual_str = str(self.datapoint_1)
        self.assertMultiLineEqual(expected_str, actual_str)

    def test_abs_diff(self):
        """Test to see if expected absolute difference is returned.
        """
        expected_abs_diff = 0.0
        actual_abs_diff = self.datapoint_1.absDiff()
        self.assertAlmostEqual(actual_abs_diff, expected_abs_diff)

    def test_unit(self):
        """Test to see if expected unit dictionary is returned.
        """
        expected_dict = {"y": Units.dimensionless, "x": Units.dimensionless}
        actual_dict = self.datapoint_1.unit()
        self.assertDictEqual(actual_dict, expected_dict)

    def test_value_names(self):
        """Test to see if expected tuple is produced.
        """
        expected_tupple = ("Log(Sw)_{Exp}", "Log(Sw)_{Calc}")
        actual_tuple = self.datapoint_1.value_names()
        self.assertEqual(expected_tupple, actual_tuple)

    def test_convert_datapoint(self):
        """Test to see if the expected converted Datapoint is returned.
        """
        expected_datapoint = self.datapoint_3
        actual_datapoint = self.datapoint_2.convertDatapoint(
            {"y": Units.kj_per_mol, "x": Units.kj_per_mol}
        )
        self.assertEqual(repr(actual_datapoint), repr(expected_datapoint))

    def test_create_datapoint(self):
        """Test to see if expected Datapoint is produced.
        """
        expected_datapoint = self.datapoint_1
        actual_datapoint = Datapoint.create_datapoint(
            [
                Value(1.0, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                Value(1.0, "Log(Sw)", ValueType.calculated, Units.dimensionless),
                "EXAMPLE",
            ]
        )
        self.assertEqual(actual_datapoint, expected_datapoint)
