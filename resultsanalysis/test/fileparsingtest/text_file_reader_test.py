#    resultsanalysis provides generic tools and classes for analysis of calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    resultsanalysis is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script contains test case for the TextFileReader
@author: mark
"""

import unittest
import logging
import pathlib
from resultsanalysis.dataclasses.units import Units
from resultsanalysis.dataclasses.value import Value
from resultsanalysis.dataclasses.valuetype import ValueType
from resultsanalysis.dataclasses.datapoint import Datapoint
from resultsanalysis.dataclasses.molecule import Molecule
from resultsanalysis.dataclasses.molecules import Molecules
import resultsanalysis.fileparsing.textfilereader as TextFileReader

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class TextFileReaderTestCase(unittest.TestCase):
    """Test case for the Datapoint object.
    """

    def setUp(self):
        """Set up for tests
        """
        parent_directory = pathlib.Path(__file__).parents[1]
        self.filename = (
            (parent_directory / "resources/test_results.csv").absolute().as_posix()
        )
        self.file_contents = TextFileReader.readCSVDataFile(self.filename, "\t")
        self.header_locs = TextFileReader.readCSVHeaders(self.file_contents[0])
        self.value_types = TextFileReader.valueTypes(self.header_locs)

    def tearDown(self):
        """Tear down for tests.
        """
        del self.file_contents
        del self.header_locs
        del self.value_types

    def test_read_csv_data_file(self):
        """Test to see if file is read in as expected.
        """
        expected_contents = [
            ["Name", "InChIKey", "Log(Sw)exp", "Smiles Canonicalised", "Log(Sw)calc"],
            [
                "2-methylpentane",
                "AFABGHUZZDYHJO-UHFFFAOYSA-N",
                "-3.7",
                "CCCC(C)C",
                "-3.8114199738",
            ],
            [
                "pentan-1-ol",
                "AMQJEAYHLZJPGS-UHFFFAOYSA-N",
                "-0.6",
                "CCCCCO",
                "-0.4111337476",
            ],
            [
                "pentan-3-ol",
                "AQIXEPGDORPWBJ-UHFFFAOYSA-N",
                "-0.2",
                "CCC(CC)O",
                "-1.00398538",
            ],
            ["propane", "ATUOYWHBWRKTHZ-UHFFFAOYSA-N", "-1.9", "CCC", "-1.7131181741"],
            [
                "propan-1-ol",
                "BDERNNFJNOPAEC-UHFFFAOYSA-N",
                "0.6",
                "CCCO",
                "0.6388444768",
            ],
            [
                "butan-2-ol",
                "BTANRVKWQNVYAZ-SCSAIBSYSA-N",
                "0.5",
                "CC[C@H](O)C",
                "-0.0458138847",
            ],
            [
                "2,4-dimethylpentane",
                "BZHMBWZPUJHVEE-UHFFFAOYSA-N",
                "-4.3",
                "CC(CC(C)C)C",
                "-3.9014090428",
            ],
            ["methanol", "OKKJLVBELUTLKV-UHFFFAOYSA-N", "1.6", "CO", "1.5328632661"],
            [
                "2-methyl-1-propanol",
                "ZXEKIIBDNHEJCQ-UHFFFAOYSA-N",
                "0.2",
                "OCC(C)C",
                "-0.0780129341",
            ],
        ]
        actual_contents = self.file_contents
        self.assertListEqual(actual_contents, expected_contents)

    def test_parser_units(self):
        """Test to see if expected unit is returned.
        """
        expected_unit = Units.dimensionless
        actual_unit = TextFileReader.parseUnit("")
        self.assertEqual(actual_unit, expected_unit)

    def test_read_csv_headers(self):
        """Test to see if headers are extracted as expected.
        """
        expected_header_locs = {
            "InChIKey": 1,
            "Name": 0,
            "SMILES": 3,
            ("Log(Sw)", "Log(Sw)"): {
                "Calc": 4,
                "Exp": 2,
                "Unit": {"y": Units.dimensionless, "x": Units.dimensionless},
            },
        }
        actual_header_locs = self.header_locs
        self.assertDictEqual(actual_header_locs, expected_header_locs)

    def test_value_types(self):
        """Test to see if expected value types are returned.
        """
        expected_values = [("Log(Sw)", "Log(Sw)")]
        actual_values = self.value_types
        self.assertListEqual(actual_values, expected_values)

    def test_create_datapoint(self):
        """Test to see if expected datapoint is created.
        """
        expected_datapoint = Datapoint.from_values(
            Value(-3.7, "Log(Sw)", ValueType.experimental, Units.dimensionless),
            Value(-3.8114199738, "Log(Sw)", ValueType.calculated, Units.dimensionless),
            "AFABGHUZZDYHJO-UHFFFAOYSA-N",
        )
        actual_datapoint = TextFileReader.createDatapoint(
            self.value_types[0], self.header_locs, self.file_contents[1]
        )
        LOGGER.debug("Actual_datapoint:")
        LOGGER.debug("%r", actual_datapoint)
        LOGGER.debug("Expected datapoint:")
        LOGGER.debug("%r", expected_datapoint)
        self.assertEqual(actual_datapoint, expected_datapoint)

    def test_create_datapoint_list(self):
        """Test to see if expected datapoint list is created.
        """
        expected_datapoint_list = [
            Datapoint.from_values(
                Value(-3.7, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                Value(
                    -3.8114199738, "Log(Sw)", ValueType.calculated, Units.dimensionless
                ),
                "AFABGHUZZDYHJO-UHFFFAOYSA-N",
            )
        ]
        actual_datapoint_list = TextFileReader.createDatapointList(
            self.value_types, self.header_locs, self.file_contents[1]
        )
        LOGGER.debug(actual_datapoint_list)
        self.assertListEqual(actual_datapoint_list, expected_datapoint_list)

    def test_create_molecule(self):
        """Test to see if expected molecule is created.
        """
        expected_molecule = Molecule.from_moldescriptors(
            "AFABGHUZZDYHJO-UHFFFAOYSA-N", "2-methylpentane", "CCCC(C)C"
        )
        expected_molecule.addDatapoint(
            Datapoint.from_values(
                Value(-3.7, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                Value(
                    -3.8114199738, "Log(Sw)", ValueType.calculated, Units.dimensionless
                ),
                "AFABGHUZZDYHJO-UHFFFAOYSA-N",
            )
        )
        datapoint_list = TextFileReader.createDatapointList(
            self.value_types, self.header_locs, self.file_contents[1]
        )
        actual_molecule = TextFileReader.createMolecule(
            self.header_locs, self.file_contents[1], datapoint_list
        )
        self.assertEqual(actual_molecule, expected_molecule)

    def test_create_molecule_list(self):
        """Test to see if expected list is produced.
        """
        expected_mol_list = [
            Molecule.create_molecule_with_datapoints(
                ["AFABGHUZZDYHJO-UHFFFAOYSA-N", "2-methylpentane", "CCCC(C)C"],
                [
                    Datapoint.from_values(
                        Value(
                            -3.7, "Log(Sw)", ValueType.experimental, Units.dimensionless
                        ),
                        Value(
                            -3.8114199738,
                            "Log(Sw)",
                            ValueType.calculated,
                            Units.dimensionless,
                        ),
                        "AFABGHUZZDYHJO-UHFFFAOYSA-N",
                    )
                ],
            )
        ]
        actual_mol_list = TextFileReader.createMoleculeList(
            self.header_locs, [self.file_contents[1]]
        )
        for index, actual_mol in enumerate(actual_mol_list):
            expected_mol = expected_mol_list[index]
            self.assertEqual(actual_mol, expected_mol)

    def test_create_molecules(self):
        """Test to see if expected molecules object is created.
        """
        mol_list = TextFileReader.createMoleculeList(
            self.header_locs, [self.file_contents[1]]
        )
        actual_molecules = TextFileReader.createMolecules(self.header_locs, mol_list)
        mol = Molecule.create_molecule_with_datapoints(
            ["AFABGHUZZDYHJO-UHFFFAOYSA-N", "2-methylpentane", "CCCC(C)C"],
            [
                Datapoint.from_values(
                    Value(-3.7, "Log(Sw)", ValueType.experimental, Units.dimensionless),
                    Value(
                        -3.8114199738,
                        "Log(Sw)",
                        ValueType.calculated,
                        Units.dimensionless,
                    ),
                    "AFABGHUZZDYHJO-UHFFFAOYSA-N",
                )
            ],
        )
        expected_mols = Molecules(data=mol.data)
        self.assertEqual(actual_molecules, expected_mols)

    def test_parse_csv_file(self):
        """Test to see if file is parsed as expected.
        """
        actual_molecules = TextFileReader.parseCSVDataFile(self.filename, "\t")
        expected_molecules_keys = [
            "AFABGHUZZDYHJO-UHFFFAOYSA-N",
            "AMQJEAYHLZJPGS-UHFFFAOYSA-N",
            "AQIXEPGDORPWBJ-UHFFFAOYSA-N",
            "ATUOYWHBWRKTHZ-UHFFFAOYSA-N",
            "BDERNNFJNOPAEC-UHFFFAOYSA-N",
            "BTANRVKWQNVYAZ-SCSAIBSYSA-N",
            "BZHMBWZPUJHVEE-UHFFFAOYSA-N",
            "OKKJLVBELUTLKV-UHFFFAOYSA-N",
            "ZXEKIIBDNHEJCQ-UHFFFAOYSA-N",
        ]
        actual_mol_keys = sorted(actual_molecules.data.index.tolist())
        self.assertListEqual(actual_mol_keys, expected_molecules_keys)
