#    resultsanalysis provides generic tools and classes for analysis of calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    resultsanalysis is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Contains the test case for the SmartsStringSorter script.

@author: mark
"""

import unittest
import logging
import pathlib
import resultsanalysis.fileparsing.smarts_string_sorter as SmartsStringSorter

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class SmartsStringSorterTestCase(unittest.TestCase):
    """Test case for the Datapoint object.
    """

    def setUp(self):
        """Set up for tests
        """
        parent_directory = pathlib.Path(__file__).parents[1]
        self.example_filename = (
            (parent_directory / "resources/SmartsStringTest.txt").absolute().as_posix()
        )
        self.smarts_frag_list = SmartsStringSorter.SmartsFragmentsList()

    def tearDown(self):
        """Tear down for tests.
        """
        del self.smarts_frag_list

    def test___str__(self):
        """Test to see if expected string is returned.
        """
        expected_string = "SmartsFragmentsList with 0 smarts substructures."
        actual_string = str(self.smarts_frag_list)
        self.assertEqual(actual_string, expected_string)

    def test_add_smarts_string(self):
        """Test to see if smarts string is added as expected.
        """
        self.smarts_frag_list.addSmartsString("Alkene", "[CX3]=[CX3]")
        expected_smarts_string_dict = {"Alkene": "[CX3]=[CX3]"}
        actual_smarts_string_dict = self.smarts_frag_list.smarts_strings
        self.assertDictEqual(actual_smarts_string_dict, expected_smarts_string_dict)
        # Need to add test of the mol form maybe?

    def test_add_smarts_string_list(self):
        """Test to see if expected smarts string list is added.
        """
        self.smarts_frag_list.addSmartsStringList([("Alkene", "[CX3]=[CX3]")])
        expected_smarts_string_dict = {"Alkene": "[CX3]=[CX3]"}
        actual_smarts_string_dict = self.smarts_frag_list.smarts_strings
        self.assertDictEqual(actual_smarts_string_dict, expected_smarts_string_dict)

    def test_read_smarts__from_file(self):
        """Test to see if expected smarts fragment list is created.
        """
        actual_smarts_list = SmartsStringSorter.readSmartsFromFile(
            self.example_filename
        )
        expected_smarts_list = [("Alkene", "[CX3]=[CX3]")]
        self.assertListEqual(actual_smarts_list, expected_smarts_list)
