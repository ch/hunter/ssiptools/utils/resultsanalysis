#    resultsanalysis provides generic tools and classes for analysis of calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    resultsanalysis is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Module containing Datapoints class.

A Datapoints object is made up of a set of Datapoint objects with the same
value_type and unit. This class is used for the bulk of numerical
manipulations. numpy.array objects are generated from the collected data.
Methods use the arrays to calculate linear regression coefficients, as well as
RMSE and R^2 for the data. Minimum and maximum values for the data can also be
returned. A sub set of the Datapoints can be extracted, for further analysis.
"""
import numpy as np
import numpy.polynomial.polynomial as poly
import scipy.stats as stats
import pandas
import logging


logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class Datapoints(object):
    """Class to hold datapoints with the same value_type attribute. Numerical
    operations are carried out on the dataset using this object.
    """

    def __init__(self, data=None):
        """Initialise with the value_type and unit for data to be contained
        in object.
        """
        self.data = data

    @classmethod
    def fromdatapoints(cls, datapoint_list):
        """This generates Datapoints object from list of datapoint objects.
        """
        LOGGER.info("datapoint list length: %i", len(datapoint_list))
        data = datapoint_list[0].data.copy()
        LOGGER.info("data length: %i", data.shape[0])
        for datapoint in datapoint_list[1:]:
            data = data.append(datapoint.data)
            LOGGER.info("data length: %i", data.shape[0])
        return Datapoints(data)

    def __repr__(self):
        """Overload repr of object.
        """
        if isinstance(self.data, pandas.DataFrame):
            return """Datapoints({0})""".format(self.data.to_string())
        else:
            return "Datapoints()"

    def __str__(self):
        """string method of Datapoints gives number of datapoint objects, unit
        and value_type.
        """
        return repr(self)

    def __eq__(self, other_datapoints):
        """Overload the equals method.
        """
        if isinstance(self.data, pandas.DataFrame) and isinstance(
            other_datapoints.data, pandas.DataFrame
        ):
            return self.data.equals(other_datapoints.data)
        else:
            return False

    def x_values(self):
        """Gets the x values.
        """
        return self.data.iloc[:, 0]

    def y_values(self):
        """Gets the y values.
        """
        return self.data.iloc[:, 2]

    def unit(self):
        """Returns dictionary of the units.
        """
        return {"x": self.data.iloc[0, 1], "y": self.data.iloc[0, 3]}

    def _sorted_datapoints_list(self):
        """Function returns a list of the Datapoint objects, with order
        based on the sorted list of keys.
        """
        return Datapoints(self.data.sort_index())

    def addDatapoint(self, datapoint):
        """add datapoint to dictionary- if value_type and unit match that of
        the Datapoints instance. molecule_id is used as key for the
        dictionary to avoid repeat entries.
        """
        if isinstance(self.data, pandas.DataFrame):
            self.data = self.data.append(datapoint.data)
        else:
            self.data = datapoint.data

    def addDatapointList(self, datapoint_list):
        """Add a list of datapoints to the object.
        """
        for datapoint in datapoint_list:
            self.addDatapoint(datapoint)

    def value_type_tuple(self):
        """Returns a tuple of the value types.
        """
        return tuple(self.data.columns.values[0::2])

    def unit_tuple(self):
        """Returns a tuple of the units.
        """
        return (tuple(self.data.columns.values[1::2]), tuple(self.data.values[0][1::2]))

    def expDataExtremeValues(self):
        """returns the min and max values of experimental values in array.
        """
        exp_min = np.amin(self.x_values())
        exp_max = np.amax(self.x_values())
        return (exp_min, exp_max)

    def calcDataExtremeValues(self):
        """returns the min and max values of calculated values in array.
        """
        calc_min = np.amin(self.y_values())
        calc_max = np.amax(self.y_values())
        return (calc_min, calc_max)

    def rSquared(self):
        """ Return pearson's R^2 of the data in the arrays.
        """
        lin_regress = stats.linregress(self.x_values(), self.y_values())
        r_value = lin_regress[2]
        return r_value ** 2

    def rmse(self):
        """This returns the RMSE of the data in the arrays.
        """
        rmse_value = np.sqrt(((self.x_values() - self.y_values()) ** 2).mean())
        return rmse_value

    def linearSlope(self):
        """Returns gradient if data is fitted to a linear equation.
        From  calc_value = slope*exp_value + intercept
        """
        lin_regress = stats.linregress(self.x_values(), self.y_values())
        slope = lin_regress[0]
        return slope

    def linearIntercept(self):
        """Returns intercept if data is fitted to a linear equation.
        From  calc_value = slope*exp_value + intercept
        """
        lin_regress = stats.linregress(self.x_values(), self.y_values())
        intercept = lin_regress[1]
        return intercept

    def regress(self):
        """This returns slope, intercept, R^2 and RMSE values, of the
        experimental and calculated values in the arrays.
        """
        rmse_value = self.rmse()
        # check to see if more than 1 datapoint in datapoint_list, otherwise
        # slope, intercept and R^2 are not calculatable- return null values for
        # all but RMSE.
        if self.data.shape[0] > 1:
            rsquared_value = self.rSquared()
            slope = self.linearSlope()
            intercept = self.linearIntercept()
        else:
            LOGGER.warn(
                "Too few datapoints for full analysis. Only RMSE will be returned."
            )
            slope = None
            intercept = None
            rsquared_value = None
        return tuple([slope, intercept, rsquared_value, rmse_value])

    def polynomial_fit_with_covar(self, order):
        """This attempts to fit polynomial with the covariance also returned
        """
        return np.polyfit(self.x_values(), self.y_values(), order, cov=True)

    def polynomial_fit(self, order):
        """This attempts to fit a polynomial to the data, of the given order.
        """
        return poly.polyfit(self.x_values(), self.y_values(), order)

    def polynomial_fit_covar(self, order):
        """This returns the total covariance for the fit between the x and y data.
        """
        return poly.polyfit(self.x_values(), self.y_values(), order, full=True)[1][0]

    def polynomial_fit_rmse(self, order):
        """This calculates the RMSE between the fitted values and the actual values.
        """
        polynomial_coefficients = self.polynomial_fit(order)
        polynomial_values = poly.polyval(self.x_values(), polynomial_coefficients)
        return np.sqrt(((polynomial_values - self.y_values()) ** 2).mean())

    def poly_fit_data(self, order):
        """This returns a dictionary containing the polynomial coefficients,
        covariance, RMSE and order of the fit.
        """
        return {
            "coefficients": self.polynomial_fit(order),
            "RMSE": self.polynomial_fit_rmse(order),
            "order": order,
            "covar": self.polynomial_fit_covar(order),
        }

    def createDatapointsSubGroup(self, molecule_id_list):
        """This takes a list of molecule_id values and returns a new Datapoints
        object, containing only the Datapoint objects with the molecule_ids.
        """
        return Datapoints(self.data.loc[molecule_id_list])

    def plotRanges(self):
        """This object uses the min, max values to create the min max values
        for axes.
        """
        exp_extreme_values = self.expDataExtremeValues()
        calc_extreme_values = self.calcDataExtremeValues()
        # we want the plot range to be slightly larger than the data range
        min_exp_value = exp_extreme_values[0]
        min_calc_value = calc_extreme_values[0]
        max_exp_value = exp_extreme_values[1]
        max_calc_value = calc_extreme_values[1]
        return [min_exp_value, max_exp_value, min_calc_value, max_calc_value]

    def generateScatterPlotParameters(self, polynomial_order=None, scales=None):
        """The method genrates a dictionarycontaining information required to
        create a matplotlib.pyplot scatter plot of the data.
        """
        scatter_plot_data = {}
        # add numpy arrays for the data
        scatter_plot_data["x_data"] = self.x_values()
        scatter_plot_data["y_data"] = self.y_values()
        # add information for the size of the axes to use- from plotRanges()
        scatter_plot_data["plot_axis_range"] = self.plotRanges()
        scatter_plot_data["x_axis_range"] = self.plotRanges()[0:2]
        scatter_plot_data["y_axis_range"] = self.plotRanges()[2:]
        # now we want to add information about the value type to the plotting
        # data, to give the correct axis titles.
        # need to improve subscripting
        x_axis_label = (
            r"$" + self.data.columns[0] + self.data.iloc[0, 1].unit_label() + "$"
        )
        y_axis_label = (
            "$" + self.data.columns[2] + self.data.iloc[0, 3].unit_label() + "$"
        )
        scatter_plot_data["x_label"] = x_axis_label
        scatter_plot_data["y_label"] = y_axis_label
        figure_label = self.data.columns[0] + "vs" + self.data.columns[2]
        figure_label = (
            figure_label.replace("{", "")
            .replace("}", "")
            .replace("\\", "")
            .replace("/", "_")
        )
        scatter_plot_data["figure_label"] = figure_label
        if polynomial_order is not None:
            scatter_plot_data["polynomial order"] = polynomial_order
            scatter_plot_data["polynomial coefficients"] = self.polynomial_fit(
                polynomial_order
            )
        if scales is not None:
            scatter_plot_data["x_scale"] = scales[0]
            scatter_plot_data["y_scale"] = scales[1]
        return scatter_plot_data
