#    resultsanalysis provides generic tools and classes for analysis of calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    resultsanalysis is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Scipt contains Value class: this contains information about the unit, and the
ability to convert between units of the same type.
@author: mdd31
"""

import logging
import numpy as np


logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class Value(object):
    """This contains a value, with a type and a unit associated with it. This
    allows for the conversion of the value to another unit.
    """

    def __init__(self, value, value_name, value_type, unit):
        """Initialise object.
        """
        self.value = value
        self.value_name = value_name
        self.value_type = value_type
        self.unit = unit
        self.error = 1e-6

    def __repr__(self):
        """Overload repr so we can reproduce object.
        """
        repr_string = "Value({self.value}, '{self.value_name}', {self.value_type}, {self.unit})".format(
            self=self
        )
        return repr_string

    def __eq__(self, other_value):
        """Overload eq so all values are the same.
        """
        return (
            np.abs(self.value - other_value.value) < self.error
            and self.unit == other_value.unit
            and self.value_name == other_value.value_name
            and self.value_type == other_value.value_type
        )

    def abs_diff(self, other_value):
        """Function returns the absolute difference between 2 Values, if they
        have the same value type and unit, else an error is raised.
        """
        if self.value_name == other_value.value_name:
            if self.unit == other_value.unit:
                return np.abs((self.value - other_value.value))
            else:
                raise TypeError("Correct value type, but units do not match.")
        else:
            raise TypeError("Value Types do not match.")

    def convert_value(self, new_unit):
        """Function returns a new value object, with the value in the new_unit.

        Raises an error if the conversion cannot be done.
        """
        try:
            conversion_factor = self.unit.conversionFactor(new_unit)
        except TypeError as err:
            raise err
        else:
            new_value = conversion_factor * self.value
            converted_value = Value(
                new_value, self.value_name, self.value_type, new_unit
            )
            return converted_value
