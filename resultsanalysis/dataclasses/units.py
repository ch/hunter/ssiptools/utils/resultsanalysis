#    resultsanalysis provides generic tools and classes for analysis of calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    resultsanalysis is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script contains enumeration for unit.

@author: mark
"""
import logging
import enum

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class Units(enum.Enum):
    """Class contains an enumeration of the different units which we can plot.
    """

    dimensionless = 1
    atomic_units_potential = 2
    volts = 3
    atomic_units_energy = 4
    kj_per_mol = 5
    kcal_per_mol = 6
    molar = 7

    def __repr__(self):
        """Overload the __repr__ of the enumeration, so it returns the string
        Units.{self.name}
        """
        repr_string = "Units.{self.name}".format(self=self)
        return repr_string

    def describe(self):
        """This returns the name and value of the enumeration.
        """
        return self.name, self.value

    def conversionFactor(self, convert_to_unit):
        """This returns the conversion factor to convert the current unit to the
        given unit, which is the convert_to_unit.

        Note that you can only convert quantities that have the same base units.

        Conversion factor sources:
        https://en.wikipedia.org/wiki/Atomic_units

        https://en.wikipedia.org/wiki/Hartree

        """
        # Don't know how to convert to a dimensionless quantity for all values.
        LOGGER.debug("self:")
        LOGGER.debug(self)
        LOGGER.debug("convert to:")
        LOGGER.debug(convert_to_unit)
        if self.value == 1 or convert_to_unit.value == 1:
            raise TypeError("Can't convert to or from dimensionless unit.")
        elif (self.value == 2 or self.value == 3) and (
            convert_to_unit.value == 2 or convert_to_unit.value == 3
        ):
            # conversion between potential units.
            if self.name == convert_to_unit.name:
                conversion_factor = 1.0
            elif (
                self.name == "atomic_units_potential"
                and convert_to_unit.name == "volts"
            ):
                conversion_factor = 27.21138505
            elif (
                self.name == "volts"
                and convert_to_unit.name == "atomic_units_potential"
            ):
                conversion_factor = 1.0 / 27.21138505
        elif ((self.value == 4) or (self.value == 5) or (self.value == 6)) and (
            convert_to_unit.value == 4
            or convert_to_unit.value == 5
            or convert_to_unit.value == 6
        ):
            if self.name == convert_to_unit.name:
                conversion_factor = 1.0
            elif (
                self.name == "atomic_units_energy"
                and convert_to_unit.name == "kj_per_mol"
            ):
                conversion_factor = 2625.49962
            elif (
                self.name == "atomic_units_energy"
                and convert_to_unit.name == "kcal_per_mol"
            ):
                conversion_factor = 627.509
            elif (
                self.name == "kj_per_mol"
                and convert_to_unit.name == "atomic_units_energy"
            ):
                conversion_factor = 1.0 / 2625.49962
            elif self.name == "kj_per_mol" and convert_to_unit.name == "kcal_per_mol":
                conversion_factor = 1.0 / 4.184
            elif (
                self.name == "kcal_per_mol"
                and convert_to_unit.name == "atomic_units_energy"
            ):
                conversion_factor = 1.0 / 627.509
            elif self.name == "kcal_per_mol" and convert_to_unit.name == "kj_per_mol":
                conversion_factor = 4.184
        else:
            raise TypeError("Can't convert between these units.")
        return conversion_factor

    def unit_label(self):
        """Function returns a formatted latex string for use in graph plotting,
        for matplotlib figures.
        """
        label = None
        if self.name == "dimensionless":
            label = ""
        elif self.name == "atomic_units_potential":
            label = "A.U."
        elif self.name == "volts":
            label = "V"
        elif self.name == "atomic_units_energy":
            label = "A.U."
        elif self.name == "kcal_per_mol":
            label = r"kcalmol^{-1}"
        elif self.name == "kj_per_mol":
            label = r"kJmol^{-1}"
        elif self.name == "molar":
            label = r"M"
        if label != "":
            return "/" + label
        else:
            return label


def createUnitsFromString(unit_string):
    """Function takes a string and converts it to the enumeration equivalent.
    """
    if unit_string == "dimensionless":
        unit = Units.dimensionless
    elif unit_string == "atomic_units_potential":
        unit = Units.atomic_units_potential
    elif unit_string == "volts":
        unit = Units.volts
    elif unit_string == "atomic_units_energy":
        unit = Units.atomic_units_energy
    elif unit_string == "kj_per_mol":
        unit = Units.kj_per_mol
    elif unit_string == "kcal_per_mol":
        unit = Units.kcal_per_mol
    return unit
