#    resultsanalysis provides generic tools and classes for analysis of calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    resultsanalysis is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Molecules file contains Molecules class.

The Molecules object is designed to hold a list of Molecule objects. A fragment
list can also be added. The fragment list is a SmartsFragmentList, containing
the functional groups of interest. The fragment list is used to find functional
groups in the molecule, and number of rings present in the molecule. Datapoints
are created for a given value type, to allow numerical manipulation of the
data.
"""

import logging
import pandas
from resultsanalysis.dataclasses.datapoints import Datapoints

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class Molecules(object):
    """Class contains a collection of Molecule objects. It can also contain
    SmartsFragmentList object.
    """

    def __init__(self, data=None):
        """Initialise an empty Molecules object.
        A Molecule object is added after initialisation.
        """
        LOGGER.debug("Initialising Molecules object.")
        self.data = data
        self.fragment_list = None

    def __repr__(self):
        """Representation of Molecules.
        """
        if isinstance(self.data, pandas.DataFrame):
            return """Moleculess({0})""".format(self.data.to_string())
        else:
            return "Molecules()"

    def __str__(self):
        """String representation contains number of Molecule and Datapoints
        objects.
        """
        return repr(self)

    def __eq__(self, other_molecule):
        """Overload equals so it checks to see if molecule has same smiles,
        inchikey and number of rings.
        """
        if isinstance(other_molecule, Molecules):
            return self._data_no_molrep().equals(other_molecule._data_no_molrep())
        else:
            return False

    def addMolecule(self, molecule):
        """method adds a molecule to the Molecules object
        """
        if isinstance(self.data, pandas.DataFrame):
            self.data = self.data.append(molecule.data)
        else:
            self.data = molecule.data

    def addMoleculeList(self, molecule_list):
        """Adds a list of molecule objects to self.
        """
        for molecule in molecule_list:
            # add the molecules using the above method
            self.addMolecule(molecule)

    def _data_no_molrep(self):
        """This returns copy of dataframe without the Mol representation, which lacks an equals operation.
        """
        data_copy = self.data.copy()
        data_copy.pop("MolRep")
        return data_copy

    def generate_datapoints(self, value_type):
        """generates a Datapoints object with value_type and unit given from
        the molecule objects present.
        """
        columns = [
            value_type[0],
            value_type[0] + "Unit",
            value_type[1],
            value_type[1] + "Unit",
        ]
        return Datapoints(data=self.data[columns])

    def addFragmentList(self, smarts_fragment_list):
        """Adds a list of definitions for functional groups/fragments.
        This should be a SmartsStringSorter.SmartsFragmentList object.
        """
        if self.fragment_list is None:
            self.fragment_list = smarts_fragment_list
        else:
            # assumes you want to overwrite definitions.
            # passes warning to logger, so user is aware.
            LOGGER.warn("overwriting fragment_list")
            self.fragment_list = smarts_fragment_list

    def _update_func_groups(self, struct_name, num_structure):
        """updates functional group information.
        """
        inchikey_list = []
        for index in self.data.index:
            if num_structure.loc[index] != 0:
                self.data.loc[index, "Functional_Groups"][
                    struct_name
                ] = num_structure.loc[index]
                inchikey_list.append(index)
        return sorted(inchikey_list)

    def findFunctionalGroups(self):
        """method uses the attached SmartsFragmentList to identify functional
        groups in molecule
        """
        # list of definitions
        smarts_definitions = self.fragment_list.smarts_mol_form
        # iterate over each molecule
        func_group_info = {}
        for sub_structure_name, sub_structure in smarts_definitions.items():
            num_structure = self.data.MolRep.copy().apply(
                lambda x: len(x.GetSubstructMatches(sub_structure))
            )
            inchikey_list = self._update_func_groups(sub_structure_name, num_structure)
            func_group_info[sub_structure_name] = inchikey_list
        return func_group_info

    def findRingSizes(self):
        """method populates self.number_of_rings by looking at the number of
        rings in each molecule.
        Key = number of rings
        Value = List of InChIKeys to corresponding molecules
        """
        ring_numbers = {}
        for inchikey, number_of_rings in self.data.Number_of_Rings.items():
            # Add value to dict
            if number_of_rings not in ring_numbers.keys():
                ring_numbers[number_of_rings] = [inchikey]
            else:
                ring_numbers[number_of_rings].append(inchikey)
        for num_rings in ring_numbers.keys():
            ring_numbers[num_rings] = sorted(ring_numbers[num_rings])
        return ring_numbers

    def get_mol_subset(self, indices):
        """This gets subset of molecules.
        """
        return Molecules(self.data.loc[indices])

    def regressionAnalysis(self, value_type):
        """Carry out regression analysis on Datapoints object with given
        value_type.
        """
        datapoints = self.generate_datapoints(value_type)
        LOGGER.debug("datapoints, number of points: %i", datapoints.data.shape[0])
        LOGGER.debug(repr(datapoints))
        regression_analysis = datapoints.regress()
        return (value_type, datapoints.unit(), regression_analysis)

    def dataByFunctionalGroup(self, value_type):
        """method returns dictionary, with key = functional group,
        value = Datapoints
        """
        datapoints_by_functional_group = {}
        all_datapoints = self.generate_datapoints(value_type)
        functional_groups = self.findFunctionalGroups()
        for functional_group, inchikey_list in functional_groups.items():
            datapoints = all_datapoints.createDatapointsSubGroup(inchikey_list)
            if datapoints.data.shape[0] > 0:
                datapoints_by_functional_group[functional_group] = datapoints
        return datapoints_by_functional_group

    def regressionAnalysisPerFunctionalGroup(self, value_type):
        """this returns a dictionary with the regression analysis based on
        functional groups.
        """
        functional_group_reg_analysis = {}
        datapoints_by_functional_group = self.dataByFunctionalGroup(value_type)
        for functional_group, datapoints in datapoints_by_functional_group.items():
            regression_data = datapoints.regress()
            functional_group_reg_analysis[functional_group] = regression_data
        return functional_group_reg_analysis

    def dataByNumberOfRings(self, value_type):
        """method returns dictionary, with key = number of rings in molecule,
        value = corresponding Datapoints object.
        """
        datapoints_by_number_of_rings = {}
        all_datapoints = self.generate_datapoints(value_type)
        for number_of_rings, inchikey_list in self.findRingSizes().items():
            LOGGER.info("number of rings %i", number_of_rings)
            LOGGER.debug("inchikey list")
            LOGGER.debug(inchikey_list)
            datapoints = all_datapoints.createDatapointsSubGroup(inchikey_list)
            LOGGER.debug("datapoints")
            LOGGER.debug(datapoints)
            if datapoints.data.shape[0] > 0:
                datapoints_by_number_of_rings[number_of_rings] = datapoints
        return datapoints_by_number_of_rings

    def regressionAnalysisPerNumberOfRings(self, value_type):
        """method returns dictionary with the regression analysis based on
        number of rings.
        """
        number_of_rings_reg_analysis = {}
        datapoints_by_number_of_rings = self.dataByNumberOfRings(value_type)
        for number_of_rings in datapoints_by_number_of_rings.keys():
            datapoints = datapoints_by_number_of_rings[number_of_rings]
            regression_data = datapoints.regress()
            number_of_rings_reg_analysis[number_of_rings] = regression_data
        return number_of_rings_reg_analysis

    def generateScatterPlotParametersPerNumberOfRings(self, value_type):
        """method generates plotting parameters for the associated value_type
        """
        # generate general plotting parameters
        plot_params_by_num_rings = self.generate_datapoints(
            value_type
        ).generateScatterPlotParameters()
        datapoints_by_number_of_rings = self.dataByNumberOfRings(value_type)
        # overwrite normal entry for figure_label:
        plot_params_by_num_rings["figure_label"] = (
            "{val[0]}vs{val[1]}ByNumRings".format(val=value_type)
            .replace("{", "")
            .replace("}", "")
        )
        # add new entry for data for individual rings.
        plot_params_by_num_rings["data_by_num_rings"] = {}
        for num_rings in datapoints_by_number_of_rings.keys():
            data_num_ring = datapoints_by_number_of_rings[num_rings]
            plot_params = data_num_ring.generateScatterPlotParameters()
            plot_params_by_num_rings["data_by_num_rings"][num_rings] = plot_params
        return plot_params_by_num_rings

    def generateScatterPlotParametersPerFunctionalGroup(self, value_type):
        """Method generates ploting parameters for the associated value_type.
        """
        plot_params_by_func_group = self.generate_datapoints(
            value_type
        ).generateScatterPlotParameters()
        datapoints_by_func_groups = self.dataByFunctionalGroup(value_type)
        # overwrite normal entry for figure_label:
        plot_params_by_func_group["figure_label"] = (
            "{val[0]}vs{val[1]}ByFuncGroup".format(val=value_type)
            .replace("{", "")
            .replace("}", "")
        )
        # add new entry for data for individual rings.
        plot_params_by_func_group["data_by_func_group"] = {}
        for func_group in datapoints_by_func_groups.keys():
            data_func_group = datapoints_by_func_groups[func_group]
            plot_params = data_func_group.generateScatterPlotParameters()
            plot_params_by_func_group["data_by_func_group"][func_group] = plot_params
        return plot_params_by_func_group

    def generateBarPlotParametersPerNumberOfRings(self, value_type):
        """method to generate the parameters for the bar chart of RMSE by
        number of rings in the molecules.
        """
        ring_reg_analysis = self.regressionAnalysisPerNumberOfRings(value_type)
        bar_plot_params_by_num_rings = {}
        bar_plot_params_by_num_rings["figure_label"] = (
            "{val[0]}vs{val[1]}rmseByRings".format(val=value_type)
            .replace("{", "")
            .replace("}", "")
        )
        ring_rmse_values = {}
        for num_rings in ring_reg_analysis.keys():
            ring_rmse_values[num_rings] = ring_reg_analysis[num_rings][3]
        bar_plot_params_by_num_rings["rmse_values"] = ring_rmse_values
        bar_plot_params_by_num_rings["unit"] = self.generate_datapoints(
            value_type
        ).unit()
        return bar_plot_params_by_num_rings

    def generateBarPlotParametersPerFunctionalGroup(self, value_type):
        """method to generate the parameters for the bar chart of RMSE by
        functional groups.
        """
        func_group_reg = self.regressionAnalysisPerFunctionalGroup(value_type)
        bar_plot_params_by_func_group = {}
        bar_plot_params_by_func_group["figure_label"] = (
            "{val[0]}vs{val[1]}rmseByFuncGroup".format(val=value_type)
            .replace("{", "")
            .replace("}", "")
        )
        func_rmse_values = {}
        for func_group in func_group_reg.keys():
            func_rmse_values[func_group] = func_group_reg[func_group][3]
        bar_plot_params_by_func_group["rmse_values"] = func_rmse_values
        bar_plot_params_by_func_group["unit"] = self.generate_datapoints(
            value_type
        ).unit()
        return bar_plot_params_by_func_group

    def generateRegressionDataToWrite(self, value_type):
        """method to generate the data required for outputting regression
        analysis to a csv file.
        """
        reg_analysis = self.generate_datapoints(value_type).regress()
        writer_params = {}
        writer_params["value_type"] = value_type
        writer_params["unit"] = self.generate_datapoints(value_type).unit()
        writer_params["Regress_values"] = reg_analysis
        writer_params["filename"] = (
            "{val[0]}vs{val[1]}Regress.csv".format(val=value_type)
            .replace("{", "")
            .replace("}", "")
        )
        return writer_params

    def generateRegressionDataToWritePerNumberOfRings(self, value_type):
        """method to generate the data required for outputting regression
        analysis performed by number of rings in the molecule to a csv file.
        """
        ring_reg_analysis = self.regressionAnalysisPerNumberOfRings(value_type)
        writer_params_num_rings = {}
        writer_params_num_rings["value_type"] = value_type
        writer_params_num_rings["unit"] = self.generate_datapoints(value_type).unit()
        writer_params_num_rings["Regress_values"] = ring_reg_analysis
        writer_params_num_rings["filename"] = (
            "{val[0]}vs{val[1]}RegressByRings.csv".format(val=value_type)
            .replace("{", "")
            .replace("}", "")
        )
        return writer_params_num_rings

    def generateRegressionDataToWritePerFunctionalGroup(self, value_type):
        """method to generate the data required for outputting regression
        analysis performed by functional group present in the molecule to a
        csv file.
        """
        func_reg_analysis = self.regressionAnalysisPerFunctionalGroup(value_type)
        writer_params_func_group = {}
        writer_params_func_group["value_type"] = value_type
        writer_params_func_group["unit"] = self.generate_datapoints(value_type).unit()
        writer_params_func_group["Regress_values"] = func_reg_analysis
        writer_params_func_group["filename"] = (
            "{val[0]}vs{val[1]}RegressByFunc.csv".format(val=value_type)
            .replace("{", "")
            .replace("}", "")
        )
        return writer_params_func_group
