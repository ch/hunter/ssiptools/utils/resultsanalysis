#    resultsanalysis provides generic tools and classes for analysis of calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    resultsanalysis is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Sscript containing type Type enum

@author: mark
"""
import logging
import enum

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class ValueType(enum.Enum):
    """Enumeration of possible value types, whether experimental, calculated or undefined
    """

    calculated = enum.auto()
    experimental = enum.auto()
    undefined = enum.auto()

    def __repr__(self):
        """Overload the __repr__ of the enumeration, so it returns the string
        ValueType.{self.name}
        """
        repr_string = "ValueType.{self.name}".format(self=self)
        return repr_string

    def describe(self):
        """This returns the name and value of the enumeration.
        """
        return self.name, self.value

    def latex_block(self):
        """This creates a block suitable for latex markdown in figure labels.
        """
        if self.name == "calculated":
            return "_{Calc}"
        elif self.name == "experimental":
            return "_{Exp}"
        else:
            return ""
