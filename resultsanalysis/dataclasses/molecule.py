#    resultsanalysis provides generic tools and classes for analysis of calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    resultsanalysis is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Molecule file contains Molecule class and functions to create a Molecule
object.

The Molecule object is designed to hold all the data relating to a particular
molecule. This includes information about properties, stored as datapoint
objects, and also structure, stored as canonicalised smiles, and rdkit.Chem.Mol
objects. Functional group and number of rings can also be added to the
information stored.

The functions in this module are designed to create Molecule objects more
easily, with the inclusion of a datapoint list from initialisation.
"""
from rdkit import Chem
import logging
import numpy as np
import pandas

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class Molecule(object):
    """
    Class to store information aboiut a molecule before
    analysis/results plotting
    """

    def __init__(self, data):
        """Initialise with InChIKey, name and Smiles
        """
        LOGGER.debug("initialising molecule.")
        self.data = data

    def __repr__(self):
        """Representation of Molecule.
        """
        return "Molecule({0})".format(self.data.to_string())

    def __str__(self):
        """String representation of Molecule Object
        """
        return repr(self)

    def __eq__(self, other_molecule):
        """Overload equals so it checks to see if molecule has same smiles,
        inchikey and number of rings.
        """
        if isinstance(other_molecule, Molecule):
            return self._data_no_molrep().equals(other_molecule._data_no_molrep())
        else:
            return False

    @classmethod
    def from_moldescriptors(cls, inchikey, name, smiles):
        """This generates the dataframe from molecule information.
        """
        column_names = [
            "name",
            "SMILES",
            "MolRep",
            "Number_of_Rings",
            "Functional_Groups",
        ]
        mol_form = Chem.MolFromSmiles(smiles)
        number_of_rings = len(Chem.GetSymmSSSR(mol_form))
        data = [[name, smiles, mol_form, number_of_rings, {}]]
        dataframe = pandas.DataFrame(data=data, columns=column_names, index=[inchikey])
        return Molecule(dataframe)

    @classmethod
    def create_molecule_with_datapoints(cls, descriptors, datapoints):
        """createMoleculeWithDatapoints generates a molecule object from
        list of ordered values to create a molecule. Datapoints from the
        inputted datapoints list is then added to the Molecule.
        """
        molecule = Molecule.from_moldescriptors(*descriptors)
        for datapoint in datapoints:
            molecule.addDatapoint(datapoint)
        return molecule

    def addDatapoint(self, datapoint):
        """Add Datapoint to molecule if the datapoint ID matches the molecule
        and it doesn't already have an entry for it-
        need to improve this, if we want to track improvements to model??
        """
        self.data = self.data.join(datapoint.data)

    def absDiffDatapoint(self, value_type):
        """return absDiff of a datapoint.
        """
        return np.abs(
            self.data.loc[self.data.index[0], value_type[0]]
            - self.data.loc[self.data.index[0], value_type[1]]
        )

    def hasDatapoint(self, value_type):
        """return true if there is a Datapoint for value_type in molecule.
        """
        if value_type[0] in self.data.columns and value_type[1] in self.data.columns:
            return True
        else:
            return False

    def searchForSubStructure(self, sub_structure):
        """SearchForSubstructure takes a Chem.Mol structure.
        Returns a Boolean. If True, sub_structure is present.
        """
        match = self.data.loc[self.data.index[0], "MolRep"].HasSubstructMatch(
            sub_structure
        )
        return match

    def searchForFunctionalGroup(self, sub_structure_name, sub_structure):
        """method searches for substructure, where sub_structure is a
        rdkit.Chem.Mol object.
        """
        match = self.searchForSubStructure(sub_structure)
        # if found it adds entry to self.functional_group with
        # key = sub_structure_name, value = num_occuur- the number of times it
        # appears in the molecule.
        if match:
            num_occur = len(
                self.data.loc[self.data.index[0], "MolRep"].GetSubstructMatches(
                    sub_structure
                )
            )
            self.data.loc[self.data.index[0], "Functional_Groups"][
                sub_structure_name
            ] = num_occur
        return sub_structure_name, match

    def _data_no_molrep(self):
        """This returns copy of dataframe without the Mol representation, which lacks an equals operation.
        """
        data_copy = self.data.copy()
        data_copy.pop("MolRep")
        return data_copy

    def searchForFunctionalGroups(self, sub_structure_dict):
        """method takes dictionary of sub_structure_name: sub_structure pairs.
        Iteratively applies searchForFunctionalGroups for all pairs
        """
        for sub_structure_name, sub_structure in sub_structure_dict.items():
            self.searchForFunctionalGroup(sub_structure_name, sub_structure)
