#    resultsanalysis provides generic tools and classes for analysis of calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    resultsanalysis is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Module containing Datapoint class and function to create Datapoint from list
of values.

This class is designed to hold a single pair of values- an experimental and
calculated value. The property and unit the data pertains to is also included.
A molecule_id is used to link the datapoint back to the system it represents,
which is usually an inchikey for a molecule.
"""

import logging
import numpy as np
import pandas

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class Datapoint(object):
    """This class stores the numerical information as two points-
    the experimental value and the calculated value
    """

    def __init__(self, dataframe):
        """Initialise the Datapoint.
        value_type- type of information in Datapoint e.g. Log(Sw), hydration
        free energy, etc.
        unit- unit of the values
        exp_value- experimental Value
        calc_value- Calculated Value
        molecule_id- ID for molecule/species the data pertains to. This is the
        InChIKey for molecule properties.
        """
        LOGGER.debug("initialising Datapoint")
        self.data = dataframe

    def __repr__(self):
        """Overload Repr so that it can be recreated.
        """
        repr_string = """Datapoint({0})""".format(self.data.to_string())
        return repr_string

    def __str__(self):
        """string method of datapoint- gives molecule, value type and unit.
        """
        to_string = self.data.to_string()
        return to_string

    def __eq__(self, other_datapoint):
        """overload eq method.
        """
        return self.data.equals(other_datapoint.data)

    @classmethod
    def from_values(cls, x_value, y_value, molecule_id):
        """This creates a DAtapoint from Value entries.
        """
        data = [[x_value.value, x_value.unit, y_value.value, y_value.unit]]
        column_names = [
            x_value.value_name + x_value.value_type.latex_block(),
            x_value.value_name + x_value.value_type.latex_block() + "Unit",
            y_value.value_name + y_value.value_type.latex_block(),
            y_value.value_name + y_value.value_type.latex_block() + "Unit",
        ]
        return Datapoint(
            pandas.DataFrame(data=data, columns=column_names, index=[molecule_id])
        )

    def absDiff(self):
        """Method returns absolute difference between calculated
        and experimental values, if they have the same unit, if not error is
        raised.
        """
        return np.abs(self.data.iloc[0, 0] - self.data.iloc[0, 2])

    def unit(self):
        """Returns dictionary of the units.
        """
        return {"x": self.data.iloc[0, 1], "y": self.data.iloc[0, 3]}

    def value_names(self):
        """Returns a tuple of the value names.
        """
        return (self.data.columns[0], self.data.columns[2])

    def convertDatapoint(self, unit_dict):
        """Returns a new Datapoint, where the values have been converted into
        the specified units.
        """
        x_conv = self.data.iloc[0, 1].conversionFactor(unit_dict["x"])
        y_conv = self.data.iloc[0, 3].conversionFactor(unit_dict["y"])
        LOGGER.debug("x conv: %.3f", x_conv)
        new_dataframe = self.data.copy()
        LOGGER.debug("x val: %.3f", new_dataframe.iloc[0, 0])
        new_dataframe.iloc[0, 0] = self.data.iloc[0, 0] * x_conv
        LOGGER.debug("x val: %.3f", new_dataframe.iloc[0, 0])
        new_dataframe.iloc[0, 2] = self.data.iloc[0, 2] * y_conv
        LOGGER.debug("x val: %.3f", new_dataframe.iloc[0, 0])
        new_dataframe.iloc[0, 1] = unit_dict["x"]
        new_dataframe.iloc[0, 3] = unit_dict["y"]
        return Datapoint(new_dataframe)

    @classmethod
    def create_datapoint(cls, data_values):
        """Function takes a list of ordered values to create Datapoint object.
        """
        datapoint = Datapoint.from_values(
            data_values[0], data_values[1], data_values[2]
        )
        return datapoint
