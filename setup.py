#    resultsanalysis provides generic tools and classes for analysis of calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    resultsanalysis is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
from setuptools import setup, find_packages

setup(name='resultsanalysis',
      use_scm_version=True,
      setup_requires=['setuptools_scm'],
      description='python scripts for doing results analysis',
      url='https://bitbucket.org/mdd31/resultsanalysis',
      author='Mark Driver',
      author_email='mdd31@cam.ac.uk',
      license='AGPLv3',
      packages=find_packages(),
      package_data={'':['SmartsStrings.txt','test/resources/*']},
      install_requires=['rdkit','numpy','pandas','scipy','matplotlib'],
      zip_safe=False)
