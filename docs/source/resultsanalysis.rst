resultsanalysis package
=======================

Subpackages
-----------

.. toctree::

   resultsanalysis.dataclasses
   resultsanalysis.fileparsing
   resultsanalysis.resultsoutput
   resultsanalysis.test

Module contents
---------------

.. automodule:: resultsanalysis
   :members:
   :undoc-members:
   :show-inheritance:
